<?php
    include('connect.php');
    session_start();
    //echo ($_SESSION['admin']);
    if (!isset($_SESSION['admin'])) {
        exit;
    }
    
    //number of product
    $sql = "SELECT * FROM products";
    //query
    $result = mysqli_query($conn, $sql);
    //number of row
    $product = mysqli_num_rows($result);
    //echo $product;

    //number of customer
    $sql = "SELECT * FROM customers";
    //query
    $result = mysqli_query($conn, $sql);
    //number of row
    $customers = mysqli_num_rows($result);

    //number of customer_order
    $sql = "SELECT * FROM customer_order";
    //query
    $result = mysqli_query($conn, $sql);
    //number of row
    $orders = mysqli_num_rows($result);

    //number of Apple
    $sql = "SELECT * FROM products WHERE brand = 'Apple'";
    //query
    $result = mysqli_query($conn, $sql);
    //number of row
    $apple = mysqli_num_rows($result);

    //number of Samsung
    //number of Apple
    $sql = "SELECT * FROM products WHERE brand = 'Samsung'";
    //query
    $result = mysqli_query($conn, $sql);
    //number of row
    $samsung = mysqli_num_rows($result);

    //number of Xiaomi
    $sql = "SELECT * FROM products WHERE brand = 'Xiaomi'";
    //query
    $result = mysqli_query($conn, $sql);
    //number of row
    $xiaomi = mysqli_num_rows($result);

    //number of Oppo
    $sql = "SELECT * FROM products WHERE brand = 'Oppo'";
    //query
    $result = mysqli_query($conn, $sql);
    //number of row
    $oppo = mysqli_num_rows($result);

    //number of Nokia
    $sql = "SELECT * FROM products WHERE brand = 'Nokia'";
    //query
    $result = mysqli_query($conn, $sql);
    //number of row
    $nokia = mysqli_num_rows($result);

    $chart_data = "{ y: 'Apple', a:".$apple."}, { y: 'Samsung', a:".$samsung."}, { y: 'Xiaomi', a:".$xiaomi."}, { y: 'Oppo', a:".$oppo."}, { y: 'Nokia', a:".$nokia."}  ";

?>
<!DOCTYPE php>
<php lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Trang Dashboard</title>
    <!-- Bootstrap -->
    <link href="./css/bootstrap.css" rel="stylesheet">
    <script src="./js/jquery-3.3.1.slim.min.js"></script>
    <script src="./js/jquery-3.4.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <!-- Font awesome -->
    <link href="./css/all.css" rel="stylesheet">
    <!-- Moris chart -->
    <link rel="stylesheet" href="./css/moris/morris-0.4.3.min.css">
    <!-- Data Table -->
    <link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css">
    <!-- Style -->
    <link rel="stylesheet" href="./css/adminstyle.css">
</head>
<body>
    <div class="container-fluid">
        <nav class="navbar navbar-expand-md bg-dark navbar-dark">
            <a class="navbar-brand col-md-3 navbarabove" style="margin-right: 0px;" href="admindashboard.php">MIMOBI</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar" style="outline:none">
                <span class="navbar-toggler-icon"></span>
            </button>
        </nav>
        
        <div class="collapse navbar-collapse sidecolor d-md-none" id="collapsibleNavbar">
            <!-- d-md-none is div hide on larger than md -->
            <ul class="nav" id="main-menu">
                <li class="text-center adminavatar">
                    <img src="./img/admin.png" alt="admin" width="70" height="70"/>
                    <p style="margin-top: 10px; margin-bottom:0px; font-size: 20px">Xin chào, Admin</p>
                </li>
                <li><a href="admin.php'" class="btn btn-danger square-btn-adjust" style="width: 60px; height: 30px; margin: 10px auto; padding:0">Thoát</a> </li>
                <li><a href="admindashboard.php"><i class="fa fa-th fa-2x" style="margin: 5px"></i>Dashboard</a></li>
                <li><a href="adminsanpham.php"><i class="fa fa-mobile-alt fa-2x" style="margin: 5px"></i>Sản phẩm</a></li>
                <li><a href="adminnguoidung.php"><i class="fa fa-users-cog fa-2x" style="margin: 5px"></i>Người dùng</a></li>
                <li><a href="admindonhang.php"><i class="fa fa-box-open fa-2x" style="margin: 5px"></i>Đơn hàng</a></li>	              	
            </ul>
        </div> 

        <div class="row">
            <!-- hide div when screen less than md -->
            <div class="col-md-3 d-none d-md-block sidecolor">
                <!-- <nav class="navbar-default navbar-side" role="navigation"> -->
                    <div class="sidebar-collapse" id="sidebar-collapse">
                        <ul class="nav">
                            <li class="text-center adminavatar">
                                <img src="./img/admin.png" alt="admin" width="70" height="70"/>
                                <p style="margin-top: 10px; margin-bottom:0px; font-size: 20px">Xin chào, Admin</p>
                            </li>
                            <li><a href="admin.php" class="btn btn-danger square-btn-adjust" style="width: 60px; height: 30px; margin: 10px auto; padding:0">Thoát</a> </li>
                            <li><a href="admindashboard.php"><i class="fa fa-th fa-2x" style="margin: 5px"></i>Dashboard</a></li>
                            <li><a href="adminsanpham.php"><i class="fa fa-mobile-alt fa-2x" style="margin: 5px"></i>Sản phẩm</a></li>
                            <li><a href="adminnguoidung.php"><i class="fa fa-users-cog fa-2x" style="margin: 5px"></i>Người dùng</a></li>
                            <li><a href="admindonhang.php"><i class="fa fa-box-open fa-2x" style="margin: 5px"></i>Đơn hàng</a></li>	              	
                        </ul>
                    </div>
                <!-- </nav> -->
            </div>
            <div class="col-md-9" style="padding:0">
                <div id="page-wrapper" >
                    <div id="page-inner">
                        <div class="row">
                            <div class="col-md-12; padding-left: 0px">
                                <h2 style="color:#f00">Dashboard</h2>   
                            </div>
                        </div>              

                        <hr />
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-6">           
                                <div class="panel panel-back noti-box">
                                    <span class="icon-box" style="background-color: #f00;border-radius: 100%;padding-top:13px">
                                        <i class="fa fa-mobile-alt"></i>
                                    </span>
                                <p class="boxtext"><?php echo $product?> Sản phẩm</p>
                            </div>
                        </div>
                            <div class="col-md-3 col-sm-6 col-xs-6">           
                                <div class="panel panel-back noti-box">
                                    <span class="icon-box" style="background-color: green;border-radius: 100%;padding-top:13px">
                                        <i class="fa fa-users-cog"></i>
                                    </span>
                                <p class="boxtext"><?php echo $customers?> Người dùng</p>
                            </div>
                        </div>
                            <div class="col-md-3 col-sm-6 col-xs-6">           
                                <div class="panel panel-back noti-box">
                                    <span class="icon-box" style="background-color: violet;border-radius: 100%;padding-top:13px">
                                        <i class="fa fa-box-open"></i>
                                    </span>
                                <p class="boxtext"><?php echo $orders?> Đơn hàng</p>
                            </div>
                        </div>
                            <div class="col-md-3 col-sm-6 col-xs-6">           
                                <div class="panel panel-back noti-box">
                                    <span class="icon-box" style="background-color: brown;border-radius: 100%;padding-top:13px">
                                        <i class="fa fa-rocket"></i>
                                    </span>
                                <p class="boxtext">5 Thương hiệu</p>
                            </div>
                        </div>
                    </div>
                            
                    <hr />
                        
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <label>SỐ LƯỢNG SẢN PHẨM</label>
                            <div id="bar-chart"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <!-- Moris chart javascript -->
    <script src="./morris/raphael-2.1.0.min.js"></script>
    <script src="./morris/morris.js"></script>

    <script>
    $(document).ready(function() {
        barChart();

    $(window).resize(function() {
        window.barChart.redraw();
        });
    });

    function barChart() {
    window.barChart = Morris.Bar({
        element: 'bar-chart',
        data: [<?php echo $chart_data; ?>],
        xkey: 'y',
        ykeys: ['a'],
        labels: ['Số lượng'],
        lineColors: ['#1e88e5'],
        lineWidth: '3px',
        resize: true,
        redraw: true
    });
    }
    </script>
    <?php
    mysqli_close($conn);
    ?>
</body>
</php>