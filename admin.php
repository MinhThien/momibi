<?php
session_start();
session_destroy();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Trang đăng nhập Admin</title>
	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- Jquery -->
	<link rel="stylesheet" href="https://jqueryvalidation.org/files/demo/site-demos.css">
	<!-- Main Style Css -->
    <link rel="stylesheet" href="./css/style_dangnhap.css"/>
</head>
<body class="form-v4">
	<div class="page-content">
		<div class="form-v4-content">
			<form class="form-detail" action="adminxulydangnhap.php" method="post" id="myform">
				<h2>ĐĂNG NHẬP ADMIN</h2>
				<div class="form-row">
					<label for="your_email">Email</label>
					<input type="text" name="emaildn" id="emaildn" class="input-text" required pattern="[^@]+@[^@]+.[a-zA-Z]{2,6}">
				</div>
				<div class="form-row ">
					<label for="password">Mật khẩu</label>
					<input type="password" name="matkhaudn" id="matkhaudn" class="input-text" required>
					
				</div>


				<br>
				<div class="form-row-last">
					<input type="submit" name="dangnhap" class="register" value="Đăng Nhập">
				</div>
			</form>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
</body>
</html>