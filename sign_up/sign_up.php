<?php
	include $_SERVER["DOCUMENT_ROOT"]."/ltw2019/connect.php";
	session_start();
	$firstname = "";
	$lastname = "";
	$email = "";
	$phone = "";
	$address = "";
	$password = "";
	$passwordconfirmation = "";
	$errors = array();

	if ($_SERVER["REQUEST_METHOD"] == "POST"){
		$firstname = mysqli_real_escape_string($conn, $_POST['first_name']);
		$lastname = mysqli_real_escape_string($conn, $_POST['last_name']);
		$email = mysqli_real_escape_string($conn, $_POST['your_email']);
		$phone = mysqli_real_escape_string($conn, $_POST['your_phone']);
		$address = mysqli_real_escape_string($conn, $_POST['address']);
		$password = mysqli_real_escape_string($conn, $_POST['password']);
		$passwordconfirmation = mysqli_real_escape_string($conn, $_POST['comfirm_password']);

		if (empty($firstname)) { array_push($errors, "first name is required");}
		if (empty($lastname)) { array_push($errors, "Last name is required");}
		if (empty($email)) { array_push($errors, "Email is required");}
		if (empty($phone)) { array_push($errors, "Phone is require");}
		if (empty($address)) { array_push($errors, "Address is require");}
		if (empty($password)) {array_push($errors, "Password is required");}
		// if (empty($passwordconfirmation)) {array_push($errors, "Password confirmation is required");}
		
		if ($password != $passwordconfirmation) {
			array_push($errors, "Passwords do not match");
		}

		$user_check_query = "SELECT * FROM customers WHERE email='$email' LIMIT 1";
		$result = mysqli_query($conn, $user_check_query);
		$user = mysqli_fetch_assoc($result);
		if($user){
			if($user['email']===$email){
				array_push($errors, "Email already exists");
			}
		} 

		if (count($errors) == 0) {
			$password = md5($password);

			$query = "INSERT INTO customers (first_name, last_name, email, phone, address, password) VALUES ('$firstname', '$lastname', '$email', '$phone', '$address', '$password')";

			mysqli_query($conn, $query);
			$_SESSION['email'] = $email;
			$_SESSION['firstname'] = $firstname;
			$_SESSION['lastname'] = $lastname;
			$_SESSION['phone'] = $phone;
			$_SESSION['address'] = $address;
			header('location: ../index.php');
		}
		
	}
?>
<!DOCTYPE php>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Trang Đăng Ký</title>
	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- Font-->
	<!-- Jquery -->
	<!-- <link rel="stylesheet" href="https://jqueryvalidation.org/files/demo/site-demos.css"> -->
	<!-- Main Style Css -->
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="sign_up.css"/>
    <link rel="stylesheet" href="../css/ass1.css">
</head>
<body class="form-v4">
	<div class="page-content">
		<div class="form-v4-content">
			<div class="form-left">
				<h2>CHÀO MỪNG BẠN <br> ĐẾN VỚI MIMOBI</h2>
				<h3 class="text-1"><b>Đăng nhập/Đăng ký để có thể:</b></h3>
				<h3 class="text-2" >- Nhận thông báo mới nhất</h3>
				<h3 class="text-2" >- Hưởng những khuyến mãi hấp dẫn</h3>
				<h3 class="text-2" >- Xem những sản phẩm tốt nhất</h3>
				<h3 class="text-2" >- Quản lý đơn hàng</h3>
				<div class="form-left-last">
					<a href="sign_in.php" class="btn btn-white already btn-animation-1 login-button">Đã có tài khoản?</a>
				</div>
			</div>
			<form class="form-detail" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post" id="myform">
				<?php include('err.php') ?>
				<h2>ĐĂNG KÝ</h2>
				<div class="form-group">
					<div class="form-row form-row-1">
						<label for="first_name">Họ</label>
						<input type="text" name="first_name" id="first_name" class="input-text" value="<?php echo $firstname; ?>">
					</div>
					<div class="form-row form-row-1">
						<label for="last_name">Tên</label>
						<input type="text" name="last_name" id="last_name" class="input-text" value="<?php echo $lastname; ?>">
					</div>
				</div>
				<div class="form-row">
					<label for="your_email">Email</label>
					<input type="text" name="your_email" id="your_email" class="input-text" required pattern="[^@]+@[^@]+.[a-zA-Z]{2,6}" value="<?php echo $email; ?>" >
				</div>
				<div class="form-row">
					<label for="your_phone">Điện thoại</label>
					<input type="text" name="your_phone" id="your_phone" class="input-text" value="<?php echo $phone; ?>">
				</div>
				<div class="form-row">
					<label for="address">Địa chỉ</label>
					<input type="text" name="address" id="address" class="input-text" value="<?php echo $address; ?>">
				</div>
				<div class="form-group">
					<div class="form-row form-row-1 ">
						<label for="password">Mật khẩu</label>
						<input type="password" name="password" id="password" class="input-text" required value="<?php echo $password; ?>">
					</div>
					<div class="form-row form-row-1">
						<label>Xác nhận mật khẩu</label>
						<input type="password" name="comfirm_password" id="comfirm_password" class="input-text" required value="<?php echo $passwordconfirmation; ?>">
					</div>
				</div>
				<div class="form-checkbox">
					<label class="container" style="padding-left:34px">Tôi đồng ý với Điều khoản dịch vụ
					  	<input type="checkbox" name="checkbox">
					  	<span class="checkmark"></span>
					</label>
				</div>
				<div class="form-row-last">
					<input type="submit" name="reg_user" class="btn btn-white btn-animation-1 login-button" value="Đăng Ký">
				</div>
			</form>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
	<!-- <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
	<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script> -->
	<script>
		// just for the demos, avoids form submit
		jQuery.validator.setDefaults({
		  	debug: true,
		  	success:  function(label){
        		label.attr('id', 'valid');
   		 	},
		});
		$( "#myform" ).validate({
		  	rules: {
			    password: "required",
		    	comfirm_password: {
		      		equalTo: "#password"
		    	}
		  	},
		  	messages: {
		  		your_email: {
		  			required: "Please provide an email"
		  		},
		  		password: {
	  				required: "Please enter a password"
		  		},
		  		comfirm_password: {
		  			required: "Please enter a password",
		      		equalTo: "Wrong Password"
		    	}
		  	}
		});
	</script>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>