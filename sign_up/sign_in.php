<?php
    include $_SERVER["DOCUMENT_ROOT"]."/ltw2019/connect.php";  
    session_start();
    $email = "";
    $password = "";
    $errors = array();
    if ($_SERVER["REQUEST_METHOD"] == "POST"){
        $email = mysqli_real_escape_string($conn, $_POST['email']);
        $password = mysqli_real_escape_string($conn, $_POST['password']);
      
        if (empty($email)) {
            array_push($errors, "Email is required");
        }
        if (empty($password)) {
            array_push($errors, "Password is required");
        }
      
        if (count($errors) == 0) {
            $password_check = md5($password);
            $query = "SELECT * FROM customers WHERE email='$email' AND password='$password_check'";
            $results = mysqli_query($conn, $query);
            $row = mysqli_fetch_assoc($results);
            if (mysqli_num_rows($results) == 1) {
              $_SESSION['email'] = $email;
              $_SESSION['firstname'] = $row['firstname'];
              $_SESSION['lastname'] = $row['lastname'];
              $_SESSION['success'] = "You are now logged in";
              header('location: ../index.php');
            }else {
                array_push($errors, "Email và password không khớp");
            }
        }
      }
?>
<!DOCTYPE php>
<php lang="en">
<head>
    <meta charset="utf-8">
    <title>Trang Đăng Nhập</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="sign_up.css"/>
    <link rel="stylesheet" href="../css/ass1.css">
</head>
<body class="form-v4">
    <?php include('err.php'); ?>
    <div class="page-content">
        
        <div class="form-v4-content">
            <div class="form-left">
                <h2>CHÀO MỪNG BẠN <br> ĐẾN VỚI MIMOBI</h2>
                <h3 class="text-1"><b>Đăng nhập/Đăng ký để có thể:</b></h3>
                <h3 class="text-2" >- Nhận thông báo mới nhất</h3>
                <h3 class="text-2" >- Hưởng những khuyến mãi hấp dẫn</h3>
                <h3 class="text-2" >- Xem những sản phẩm tốt nhất</h3>
                <h3 class="text-2" >- Quản lý đơn hàng</h3>
                <div class="form-left-last">
                    <a href="sign_up.php" class="btn btn-white already btn-animation-1 login-button">Chưa có tài khoản?</a>
                </div>
            </div>
            <form class="form-detail" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post" id="myform">
                <h2>ĐĂNG NHẬP</h2>
                
                <div class="form-row">
                    <label for="your_email">Email</label>
                    <input type="text" name="email" class="input-text" required pattern="[^@]+@[^@]+.[a-zA-Z]{2,6}" >
                </div>
                <div class="form-row form-row-1 ">
                    <label for="password">Mật khẩu</label>
                    <input type="password" name="password" id="password" class="input-text" required>
                </div>
                <div class="form-row-last">
                    <input type="submit" name="login_user" class="btn btn-white btn-animation-1 login-button" value="Đăng Nhập">
                </div>
            </form>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>