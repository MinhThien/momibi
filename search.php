<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Trang Tìm Kiếm</title>
	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- Font-->
	<!-- Jquery -->
	<!-- <link rel="stylesheet" href="https://jqueryvalidation.org/files/demo/site-demos.css"> -->
	<!-- Main Style Css -->
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/searchup.css"/>
</head>
<body class="form-v4">
	<div class="page-content">
		<div class="form-v4-content">
			<form class="form-detail" action="ketqua.php" method="post" id="myform">
				<h2 style="text-align: center;">TÌM KIẾM SẢN PHẨM</h2>
				<br>
				<div class="form-row">
					<!-- <label>Nhập từ khóa cần tìm</label> -->
					<input type="text" name="searchword" id="searchword" class="input-text" placeholder="Nhập từ khóa cần tìm">
				</div>
				<div class="form-group">
					<div class="form-checkbox">
						<label class="container" style="padding-left:34px; padding-right: 34px;">Apple
							  <input type="checkbox" name="brand_list[]" value="apple">
							  <span class="checkmark"></span>
						</label>
					</div>
					<div class="form-checkbox">
						<label class="container" style="padding-left:34px; padding-right: 34px;">Samsung
							  <input type="checkbox" name="brand_list[]" value="samsung">
							  <span class="checkmark"></span>
						</label>
					</div>
					<div class="form-checkbox">
						<label class="container" style="padding-left:34px; padding-right: 34px;">Oppo
							  <input type="checkbox" name="brand_list[]" value="oppo">
							  <span class="checkmark"></span>
						</label>
					</div>
					<div class="form-checkbox">
						<label class="container" style="padding-left:34px; padding-right: 34px;">Xiaomi
							  <input type="checkbox" name="brand_list[]" value="xiaomi">
							  <span class="checkmark"></span>
						</label>
					</div>
					<div class="form-checkbox">
						<label class="container" style="padding-left:34px; padding-right: 34px;">Nokia
							  <input type="checkbox" name="brand_list[]" value="nokia">
							  <span class="checkmark"></span>
						</label>
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="form-checkbox">
						<label class="container" style="padding-left:34px; padding-right: 34px;">> 20 triệu
							  <input type="checkbox" name="range-price" value="4">
							  <span class="checkmark"></span>
						</label>
					</div>
					<div class="form-checkbox">
						<label class="container" style="padding-left:34px; padding-right: 34px;">15 - 20 triệu
							  <input type="checkbox" name="range-price" value="3">
							  <span class="checkmark"></span>
						</label>
					</div>
					<div class="form-checkbox">
						<label class="container" style="padding-left:34px; padding-right: 34px;">10 - 15 triệu
							  <input type="checkbox" name="range-price" value="2">
							  <span class="checkmark"></span>
						</label>
					</div>
					<div class="form-checkbox">
						<label class="container" style="padding-left:34px; padding-right: 34px;">5 - 10 triệu
							  <input type="checkbox" name="range-price" value="1">
							  <span class="checkmark"></span>
						</label>
					</div>
					<div class="form-checkbox">
						<label class="container" style="padding-left:34px; padding-right: 34px;">&lt; 5 triệu
							  <input type="checkbox" name="range-price" value="0">
							  <span class="checkmark"></span>
						</label>
					</div>
				</div>
				
				<div class="form-row-last">
					<input type="submit" class="btn btn-white btn-animation-1 searchbtn">
				</div>
			</form>
		</div>
	</div>

</body>
<script>
	$( document ).ready(function() {
		$("input[name='range-price']").change(function(){
			if( $(this).is(':checked') ){
				$("input[name='range-price']").prop('checked', false);
				$(this).prop('checked', true);
			}
		})
	});
</script>
</html>