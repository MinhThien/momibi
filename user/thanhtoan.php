<?php
include $_SERVER["DOCUMENT_ROOT"]."/ltw2019/connect.php";
mysqli_set_charset($conn,'utf8');

session_start(); 
if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['email']);
    header("location: ../index.php");
}

if (isset($_SESSION['email'])) {
    $_email = $_SESSION['email'];
    $user = $conn->query("SELECT * FROM customers WHERE email='$_email'");
    $row_user = mysqli_fetch_assoc($user);


}
?>
<!DOCTYPE php>
<php lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../css/ass1.css">
    <title>Trang Người Dùng</title>
    <link rel="stylesheet" type="text/css" href="../css/ass1.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>	
    <link rel="stylesheet" href="user.css">
    
    <script>
    $(document).ready(function(){
        $('.slider').bxSlider();
    });
    </script>
    <style>
        .hinhanh{
            width: 105px;
            height: 100px;
            margin-bottom: 40px;
        }
        .name_product{
            font-size: 28px;
            color: red;
            width: 240px;
            padding-left: 86px;
        }
        .price{
            width: 100%;
            text-align: right;
        }
    </style>
    </head>
<body>
    <div class="containers">
        <div class="test-header">
            <div class="logo-container d-none d-sm-block">
                <a href="../index.php"><img src="../img/mimobi.png" alt="" height="87"></a>
            </div>
            <div class="logo-container d-sm-none">
                <a href="../index.php"><img src="../img/mimobismall.png" alt="" height="87"></a>
            </div>
            <div class="header-button">
                <a href="../search.php"><i class="fa fa-search searching" style="font-size: 25px; padding-right: 10px;"></i></a>
                <?php  if (isset($_SESSION['email'])) : ?>
                    <a href="user_page.php"><?php echo $row_user['last_name']; ?></a>
                    <a href="../index.php?logout='1'" class="btn btn-white btn-animation-1 login-button">Đăng Xuất </a>
                <?php endif ?>
            </div>
        </div>    
    </div>

    <div class="container">
        <br>
        <div class="your_products" style="text-align:center;font-weight:bold;font-size:2em;"><span>Thanh Toán Sản Phẩm</span></div>
        <hr> 
        <?php
            $customer_id = $row_user['id'];
            $product = $conn->query("SELECT * FROM kart_items WHERE customer_id='$customer_id'");
            if($product->num_rows >0 ){ ?>
                <?php $total = 0;?>
                <table>
                    <thead>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </thead>
                    
                    <tbody>
                     <?php 
                     
                     while($row_kart = $product-> fetch_assoc()){?>
                        <tr>
                            <td><img class="hinhanh" src="../products/<?php echo $row_kart['detail_img1']?>" alt="" ></td>
                            <td class="name_product" style="padding-left:30px"><?php echo $row_kart['name'];?></td>
                            <td class="price" style="width:80%"><?php echo number_format($row_kart['price']);?> VNĐ</td>
                            <td class="number" style="width:20%;text-align:center"><?php echo "SL: ".$row_kart['number'];?></td>
                        </tr>
                        <?php $total = $total + $row_kart['price']*$row_kart['number'];?>
                        <?php }?>
                    </tbody>
                    
                </table>
                <hr>
                    <div class="row thanhtoan">
                        <div class="col-6 address" style="float:left">
                            <p>Địa chỉ của bạn: <?php echo $row_user['address']; ?></p>
                            <p>Số điện thoại: <?php echo $row_user['phone']; ?></p>
                            <a href="pay.php" class="btn btn-white btn-animation-1">Thanh Toán</a>
                        </div>
                        <div class="col-6 total" style="float:right">
                            <p>Tổng cộng : <?php echo number_format($total); ?> VNĐ</p>
                            <p>Phí vận chuyển: 50,000 VNĐ</p>
                            <p>Phương thức thanh toán: Tiền mặt</p>
                            <p>Số tiền cần thanh toán: <span  style="color:red"><?php echo number_format($total + 50000) ;?> VNĐ</span></p>
                        </div>
                    </div>
                    
            <?php }else{?>
                <p>Không có sản phẩm trong cart</p>
            <?php } ?>

    </div>
</body>
</html>