<?php
	include $_SERVER["DOCUMENT_ROOT"]."/ltw2019/connect.php";
	mysqli_set_charset($conn,'utf8');
	session_start();

	if(isset($_SESSION['email'])){
		$_email = $_SESSION['email'];
		$query = "SELECT * FROM customers WHERE email='$_email'";
		$result = mysqli_query($conn, $query);
		$row = mysqli_fetch_assoc($result);

		$firstname = $row['first_name'];
		$lastname = $row['last_name'];
		$email = $row['email'];
		$address = $row['address'];
		$phone = $row['phone'];
		$password = '';
		$errors = [];
		if ($_SERVER["REQUEST_METHOD"] == "POST"){
			$firstname = mysqli_real_escape_string($conn, $_POST['first_name']);
			$lastname = mysqli_real_escape_string($conn, $_POST['last_name']);
			$email = mysqli_real_escape_string($conn, $_POST['email']);
			$phone = mysqli_real_escape_string($conn, $_POST['phone']);
			$address = mysqli_real_escape_string($conn, $_POST['address']);
			$password = mysqli_real_escape_string($conn, $_POST['password']);

			if (empty($firstname)) { array_push($errors, "first name is required");}
			if (empty($lastname)) { array_push($errors, "Last name is required");}
			if (empty($email)) { array_push($errors, "Email is required");}
			if (empty($phone)) { array_push($errors, "Phone is require");}
			if (empty($address)) { array_push($errors, "Address is require");}
			if (empty($password)) { array_push($errors, "Password is required");}

			if(count($errors) == 0) {
				$check_pass = md5($password);
				if($check_pass != $row['password']){
					array_push($errors, "Password is incorrest");
				}else {
					$id = $row['id'];
					if (isset($id)){
						$sql = "UPDATE customers
						SET first_name='$firstname', last_name='$lastname', email='$email', address='$address', phone='$phone', password='$check_pass'
						WHERE id='$id'";
						if(mysqli_query($conn, $sql)){
							$_SESSION['email'] = $email;
							header('location: user_page.php');
						}
					}
				}
			}
		}
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Trang Chỉnh Sửa</title>
	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- Font-->
	<!-- Jquery -->
	<!-- <link rel="stylesheet" href="https://jqueryvalidation.org/files/demo/site-demos.css"> -->
	<!-- Main Style Css -->
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="chinhsua.css"/>
    <link rel="stylesheet" href="../css/ass1.css">
</head>
<body class="form-v4">
	<?php include('errors.php') ?>
	<div class="page-content">
		<div class="form-v4-content">
			<form class="form-detail" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post" id="myform">
				<h2>THÔNG TIN CÁ NHÂN</h2>
				<div class="form-group">
					<div class="form-row form-row-1">
						<label for="first_name">Họ</label>
						<input type="text" name="first_name" class="input-text" value="<?php echo $row['first_name']; ?>">
					</div>
					<div class="form-row form-row-1">
						<label for="last_name">Tên</label>
						<input type="text" name="last_name" class="input-text" value="<?php echo $row['last_name']; ?>">
					</div>
				</div>
				<div class="form-row">
					<label for="your_email">Email</label>
					<input type="text" name="email" class="input-text" readonly  required pattern="[^@]+@[^@]+.[a-zA-Z]{2,6}" value="<?php echo $row['email']; ?>">
				</div>
				<div class="form-group">
					<div class="form-row form-row-1 ">
						<label for="your_phone">Điện Thoại</label>
						<input type="text" name="phone" class="input-text" value="<?php echo $row['phone']; ?>">
					</div>
					<div class="form-row form-row-1">
						<label>Mật khẩu</label>
						<input type="password" name="password" class="input-text">
					</div>
				</div>
				<div class="form-row">
					<label for="your_address">Địa chỉ</label>
					<input type="text" name="address" class="input-text" value="<?php echo $row['address']; ?>">
				</div>
				<div class="form-group">
					<div class="form-row form-row-1" style="text-align: center;">
						<input type="submit" name="change" class="btn btn-white btn-animation-1 login-button" style="text-align: center; width: 120px;
    text-align: center;
    text-decoration: none;
    text-transform: uppercase;
    background: #ffffff;
    position: absolute;
    padding: 20px 40px;
    border-radius: 100px;
    display: inline-block;" value="Chỉnh Sửa">
						<!-- <a href="chinhsua.html" class="btn btn-white btn-animation-1 login-button" style="width:70px;text-align: center">Sửa</a> -->
					</div>
					<div class="form-row form-row-1" style="text-align: left;">
						<!-- <input type="submit" name="back" class="btn btn-white btn-animation-1 login-button" value="Hủy"> -->
						<a href="user_page.php" class="btn btn-white btn-animation-1 login-button" style="width:70px;text-align: center;">Hủy</a>
					</div>
				</div>
				<br><br>
			</form>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
	<!-- <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
	<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script> -->
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>