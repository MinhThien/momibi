<?php
include $_SERVER["DOCUMENT_ROOT"]."/ltw2019/connect.php";
mysqli_set_charset($conn,'utf8');

session_start(); 
if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['email']);
    header("location: ../index.php");
}

if (isset($_SESSION['email'])) {
    $_email = $_SESSION['email'];
    $user = $conn->query("SELECT * FROM customers WHERE email='$_email'");
    $row_user = mysqli_fetch_assoc($user);
}
?>
<!DOCTYPE php>
<php lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../css/ass1.css">
    <title>Trang Người Dùng</title>
    <link rel="stylesheet" type="text/css" href="../css/ass1.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>	
    <link rel="stylesheet" href="user.css">
    
    <script>
    $(document).ready(function(){
        $('.slider').bxSlider();
    });
    </script>
    </head>
<body>
    <div class="containers">
    <?php if(isset($_SESSION['order'])){
        if($_SESSION['order'] != ""){?>
            <div id="time" class="alert alert-success" role="alert">
                <?php echo $_SESSION['order']; ?>
            </div>
        <?php } ?>
    <?php } ?>
        <div class="test-header">
        <div class="logo-container d-none d-sm-block">
            <a href="../index.php"><img src="../img/mimobi.png" alt="" height="87"></a>
        </div>
        <div class="logo-container d-sm-none">
            <a href="../index.php"><img src="../img/mimobismall.png" alt="" height="87"></a>
        </div>
        <div class="header-button">
            <a href="../search.php"><i class="fa fa-search searching" style="font-size: 25px; padding-right: 10px;"></i></a>
            <?php  if (isset($_SESSION['email'])) : ?>
                <a href="user_page.php"><?php echo $row_user['last_name']; ?></a>
                <a href="../index.php?logout='1'" class="btn btn-white btn-animation-1 login-button">Đăng Xuất </a>
            <?php endif ?>
        </div>
    </div>    
    </div>
    <!--  -->
    <div class="product-info">
        <div class="container-fluid">
            <div class="row title-product info-user">
                <h4 class="title">Thông tin người dùng</h4>
            </div>
            <div class="row colum">
                <div class="col-lg-2">
                    
                </div>
                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 avatar">
                    <img src="user1.jpeg" alt="">
                </div>
                <div class="col-xs-1 col-sm-2 col-md-3 tablet">
                    
                </div>
                <div class="col-lg-2 col-md-3 col-sm-2 col-xs-1 form-v4-content iphone_key">
                    <ul class="list-unstyled list1">
                        <li><p class="sub-text"><span>Name</span></p></li>
                        <li><p class="sub-text"><span>Email</span></p></li>
                        <li><p class="sub-text"><span>Số điện thoại</span></p></li>
                        <li><p class="sub-text"><span>Địa chỉ</span></p></li>
                    </ul>
                </div>
                <div class="col-lg-4 col-md-4 form-v4-content iphone_value">
                    <ul class="list-unstyled list2">
                        <li><p class="sub-text"><span><?php echo $row_user['first_name'] ." ". $row_user['last_name'];?></span></p></li>
                        <li><p class="sub-text"><span><?php echo $row_user['email'];?></span></p></li>
                        <li><p class="sub-text"><span><?php echo $row_user['phone'];?></span></p></li>
                        <li><p class="sub-text"><span><?php echo $row_user['address']; ?></span></p></li>
                    </ul>
                </div>
                <div class="col-lg-1">
                    
                </div>
            </div>
            <div class="change_infor">
                <a href="chinhsua.php"><span class="btn btn-danger change_avatar">Thay đổi</span></a>
                <!-- <a href="chinhsua.php" class="btn btn-danger change_avatar">Thay đổi</a> -->
            </div>
            <br>
        </div>
    </div>
    <!--  -->

    <div class="product-1 rm-padding " id="Apple">
                <div class="box-middle">
                    <div class="box-clear">
                        <div class="box-caption">
                            <div class="form-box kart">
                                <h2>Giỏ Hàng</h2>
                                <hr class="divider-box restyle-divider-box">
                                <img src="kart.png" alt="">
                            </div>
                            <div class="form-group">
                                <a href="thanhtoan.php" class="pay_all btn btn-white btn-animation-1 button-product fix-button">Thanh toán tất cả</a>
                            </div>
                        </div>
                        
                        <div class="all_product box-img d-lg-block d-none">
                            <div class="slider-area">
                                <div class="block-slider block-slider4 control-slider">
                                <?php
                                    $customer_id = $row_user['id'];
                                    $kart = $conn->query("SELECT * FROM kart_items WHERE customer_id='$customer_id'");
                                    if($kart->num_rows > 0){
                                    ?>
                                    <ul class="slider" id="bxslider-home4">
                                        <?php  while($row_item = $kart->fetch_assoc()){
                                            $name = $row_item['name'];
                                            $product = $conn->query("SELECT * FROM products WHERE name='$name'");
                                            $row_product = $product->fetch_assoc();
                                            $product_id = $row_product['id'];
                                            ?>
                                                <li>
                                                    <img src="../products/<?php echo $row_item['avatar']?>" alt="Slide">
                                                    <div class="caption-group kart_item">
                                                        <div class="infor_product_kart">
                                                            <h2 class="caption title fix-color">
                                                                <span class="primary"><?php echo $row_item['name'];?></span>
                                                            </h2>
                                                            <p class="item-price-child"><?php echo number_format($row_item['price']).' VND';?></p>
                                                        </div>
                                                            
                                                        <div class="action_kart">
                                                            <a href="remove_kart_item.php?id=<?php echo $row_item['id'];?>"
                                                                class="btn btn-white btn-animation-1 button_action cancel" style="margin-left: 28px !important; background: #8c8c7f">Huỷ</a>
                                                            <a href="../detail.php?id=<?php echo $product_id;?>" class="button_action btn btn-white btn-animation-1 detail" style="margin-left: 2px !important; margin-right: 29px;">Chi tiết</a>
                                                        </div>
                                                        
                                                    </div>
                                                 </li>
                                                <?php } ?>
                                    </ul>
                                    <?php }else{ ?>
                                        <div class="no_product" style="font-weight: bold !important; padding: 123px 270px;">
                                            <h1>NO PRODUCT IN CART</h1>
                                        </div>
                                            <?php } ?>
                                </div>
                            </div> <!-- End slider area -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-top-area">
                <div class="zigzag-bottom"></div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3 col-sm-6">
                                <div class="footer-about-us">
                                    <h2>Mi<span>mobi</span></h2>
                                    <p>Mimobi là website chuyên mua bán và trao đổi điện thoại di động<br>
                                        Với tiêu chí đặt chất lượng và niềm tin lên trên hết, chúng tôi mong muốn
                                        mang đến cho khách hàng những sản phẩm tốt nhất
                                    </p>
                                    <div class="footer-social">
                                        <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                                        <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                                        <a href="#" target="_blank"><i class="fa fa-youtube"></i></a>
                                        <a href="#" target="_blank"><i class="fa fa-linkedin"></i></a>
                                    </div>
                                </div>
                            </div>
        
                            <div class="col-md-3 col-sm-6">
                                <div class="footer-menu">
                                    <h2 class="footer-wid-title">Người dùng </h2>
                                    <ul>
                                        <li><a href="user_page.php">Tài khoản</a></li>
                                        <li><a href="user_page.php#Apple">Giỏ hàng</a></li>
                                        <li><a href="../search.php">Tìm kiếm</a></li>
                                        <li><a href="../allproductspage.php">Tất cả sản phẩm</a></li>
                                        <li><a href="../index.php">Trang chủ</a></li>
                                    </ul>
                                </div>
                            </div>
        
                            <div class="col-md-3 col-sm-6">
                                <div class="footer-menu">
                                    <h2 class="footer-wid-title">Nhãn hàng</h2>
                                    <ul>
                                        <li><a href="../allproductspage.php#all-apple">Apple</a></li>
                                        <li><a href="../allproductspage.php#all-nokia">Nokia</a></li>
                                        <li><a href="../allproductspage.php#all-samsung">SamSung</a></li>
                                        <li><a href="../allproductspage.php#all-xiaomi">Xiaomi</a></li>
                                        <li><a href="../allproductspage.php#all-oppo">Oppo</a></li>
                                    </ul>
                                </div>
                            </div>
        
                            <div class="col-md-3 col-sm-6">
                                <div class="footer-newsletter">
                                    <h2 class="footer-wid-title">Thông báo</h2>
                                    <p>Đăng kí dịch vụ thông báo của chúng tôi để có thể nhận những thông báo mới nhất từ website</p>
                                    <div class="newsletter-form">
                                        <a href="../sign_up/sign_up.php">
                                            <p><input type="submit" class="btn btn-primary" value="Đăng Kí"></p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- End footer top area -->
                <div class="footer-bottom-area">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="copyright">
                                    <p>&copy; 2019 Lập Trình Web. All Rights Reserved. <a href="#"
                                            target="_blank">Nhóm 5</a></p>
                                </div>
                            </div>
        
                            <div class="col-md-4">
                                <div class="footer-card-icon">
                                    <i class="fa fa-cc-discover"></i>
                                    <i class="fa fa-cc-mastercard"></i>
                                    <i class="fa fa-cc-paypal"></i>
                                    <i class="fa fa-cc-visa"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- End footer bottom area -->

</body>
</html>