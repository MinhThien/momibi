<?php
include('connect.php');
mysqli_set_charset($conn,'utf8');
$id = $_GET['id'];

session_start(); 
if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['email']);
    header("location: index.php");
}

if (isset($_SESSION['email'])) {
    $_email = $_SESSION['email'];
    $user = $conn->query("SELECT * FROM customers WHERE email='$_email'");
    $row_user = mysqli_fetch_assoc($user);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Trang Chi Tiết</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/ass1.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

</head>
<body>
    <div class="containers">
    <?php
        //get rows query
        $query = $conn->query("SELECT * FROM products WHERE id = $id ");
        if($query->num_rows > 0){ 
            while($row = $query->fetch_assoc()){
        ?>
            <div id="header">
                    <div class="inner-header">
                            <div class="body-detail">
                                    <div class="product-backgroud" style="background-image: url('products/<?php echo $row["avatar"]; ?>'); background-repeat: no-repeat;">
                                    <!-- <img src="products/<?php echo $row["avatar"]; ?>"> -->
                                    </div>
                                    <div class="box-text-wrap">
                                            <div class="box-text">
                                                <div class="title-wrap">
                                                    <h1 class="title-img" style="color:whitesmoke; font-size: 80px"><?php echo $row["name"]; ?></h1>
                                                    <p class="title-img-sub fa fa-mobile " style="color: whitesmoke;"></p>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                        <div class="test-header">
                            <div class="logo-container d-none d-sm-block">
                                <a href="index.php"><img src="img/mimobi.png" alt="" height="87"></a>
                            </div>
                            <div class="logo-container d-sm-none">
                                <a href="index.php"><img src="img/mimobismall.png" alt="" height="87"></a>
                            </div>
                            <div class="header-button">
                                <a href="search.php"><i class="fa fa-search searching" style="font-size: 25px; padding-right: 20px;"></i></a>
                                <?php  if (isset($_SESSION['email'])) : ?>
                            <a href="user/user_page.php"><?php echo $row_user['last_name']; ?></a>
                            <a href="index.php?logout='1'" class="btn btn-white btn-animation-1 login-button">Đăng Xuất </a>
                        <?php else : ?>
                            <a href="sign_up/sign_in.php" class="btn btn-white btn-animation-1 login-button" id="signin">Đăng Nhập</a>
                            <a href="sign_up/sign_up.php" class="btn btn-white btn-animation-1 signup-button" id="signup">Đăng Kí</a>
                        <?php endif ?>
                            </div>
        
                        </div>
                    </div>
                </div>
        
        <div class="menu-detail">
            <div class="child-container">
                <div class="row">
                    <div class="sumary-info">
                        <ol class="breadcrumb">
                            <li class=""><a href="index.php" style="font-family: Arial, Helvetica, sans-serif;padding-right: 7px;">Home</a><p class="fa fa-reply pdri"></p></li>
                            <li><a href="allproductspage.php" style="font-family: Arial, Helvetica, sans-serif;padding-right: 7px;">Danh mục</a><p class="fa fa-reply pdri"></p></li>
                            <li>
                                <a href="allproductspage.php#all-apple" style="font-family: Arial, Helvetica, sans-serif;padding-right: 7px;"><?php echo $row["brand"]; ?></a><p class="fa fa-reply pdri"></p></li>
                            <li class="active">
                                <a href="#" style="font-family: Arial, Helvetica, sans-serif;padding-right: 7px;"><?php echo $row["name"]; ?></a>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="product-info">
            <div class="container-fluid">
                <div class="row title-product">
                    <h4 class="title">Thông số sản phẩm</h4>
                </div>
                <div class="row colum">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <ul class="list-unstyled">
                                <li><p class="sub-text"><i class="fa fa-camera pdr"><span class="arialstyle">Camera : <?php echo $row["camera"]; ?></span></i></p></li>
                                <li><p class="sub-text"><i class="	fa fa-hdd-o pdr"><span class="arialstyle">RAM : <?php echo $row["ram"]; ?> GB</span></i></p></li>
                                <li><p class="sub-text"><i class="fa fa-microchip pdr"><span class="arialstyle">HĐH : <?php echo $row["os"]; ?></span></i></p></li>
                            </ul>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <ul class="list-unstyled">
                                <li><p class="sub-text"><i class="	fa fa-wifi pdr" ><span class="arialstyle">Kích thước: <?php echo $row["screensize"]; ?> inch</span></i></p></li>
                                <li><p class="sub-text"><i class="fa fa-power-off pdr"><span class="arialstyle">Bộ nhớ : <?php echo $row["memory"]; ?> GB</span></i></p></li>
                                <li><p class="sub-text"><i class="fa fa-gamepad pdr"><span class="arialstyle">Pin : <?php echo $row["battery"]; ?> mAh</span></i></p></li>
                            </ul>
                        </div>
                </div>
            </div>
        </div>
        <div class="product-1">
            <div class="box-middle">
                <div class="box-clear">
                    <div class="box-caption">
                        <div class="form-box">
                            <h5 class="form-title">16GB</h5>
                            <hr class="divider-box restyle-divider-box">
                            <p class="sub-text black-color">Khuyến mãi</p>
                            <p class="sub-text black-color"> <?php echo $row["description"]; ?></p>
                        </div>
                        <div class="form-group">
                            <?php echo '<a href="cartAction.php?id=',$id,'" class="btn btn-white btn-animation-1 button-product">Thêm vào giỏ hàng</a>'; ?>
                        </div>
                    </div>
                    <div class="box-img d-none d-md-block" style="padding-left:200px">
                    <img src="products/<?php echo $row["detail_img1"]; ?>">
                    </div>
                </div>
            </div>
        </div>
        <div class="product-1">
                <div class="box-middle">
                    <div class="box-clear">
                        <div class="box-caption reverse-right">
                            <div class="form-box">
                                <h5 class="form-title">32GB</h5>
                                <hr class="divider-box restyle-divider-box">
                                <p class="sub-text black-color">Khuyến mãi</p>
                                <p class="sub-text black-color">Giảm thêm 3% (210.000₫) cho khách mua online có sinh nhật trong tháng 10 Xem chi tiết *</p>
                            </div>
                            <div class="form-group">
                            <?php echo '<a href="cartAction.php?id=',$id,'" class="btn btn-white btn-animation-1 button-product">Thêm vào giỏ hàng</a>'; ?>
                            </div>
                        </div>
                        <div class="box-img reverse-left  d-none d-md-block">
                        <img src="products/<?php echo $row["detail_img2"]; ?>">
                        </div>
                    </div>
                </div>
        </div>
        
        <?php
            include('footer.php');
        ?>

        <?php } }else{ ?>
        <p>Product(s) not found.....</p>
        <?php } ?>
    </div>
</body>
</html>