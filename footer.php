<div class="footer-top-area">
            <div class="zigzag-bottom"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <div class="footer-about-us">
                            <h2>Mi<span>mobi</span></h2>
                            <p>Mimobi là website chuyên mua bán và trao đổi điện thoại di động<br>
                                Với tiêu chí đặt chất lượng và niềm tin lên trên hết, chúng tôi mong muốn
                                mang đến cho khách hàng những sản phẩm tốt nhất
                            </p>
                            <div class="footer-social">
                                <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                                <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                                <a href="#" target="_blank"><i class="fa fa-youtube"></i></a>
                                <a href="#" target="_blank"><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6">
                        <div class="footer-menu">
                            <h2 class="footer-wid-title">Người dùng </h2>
                            <ul>
                                <?php if (isset($_SESSION['email'])){?>
                                    <li><a href="user/user_page.php">Tài khoản</a></li>
                                    <li><a href="user/user_page.php#Apple">Giỏ hàng</a></li>
                                <?php } else {?>
                                    <li><a href="sign_up/sign_up.php">Đăng Ký</a></li>
                                    <li><a href="sign_up/sign_in.php">Đăng Nhập</a></li>
                                <?php } ?> 
                                <li><a href="./search.php">Tìm kiếm</a></li>
                                <li><a href="./allproductspage.php">Tất cả sản phẩm</a></li>
                                <li><a href="./index.php">Trang chủ</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6">
                        <div class="footer-menu">
                            <h2 class="footer-wid-title">Nhãn hàng</h2>
                            <ul>
                                <li><a href="allproductspage.php#all-apple">Apple</a></li>
                                <li><a href="allproductspage.php#all-nokia">Nokia</a></li>
                                <li><a href="allproductspage.php#all-samsung">SamSung</a></li>
                                <li><a href="allproductspage.php#all-xiaomi">Xiaomi</a></li>
                                <li><a href="allproductspage.php#all-oppo">Oppo</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6">
                        <div class="footer-newsletter">
                            <h2 class="footer-wid-title">Thông báo</h2>
                            <p>Đăng kí dịch vụ thông báo của chúng tôi để có thể nhận những thông báo mới nhất từ website</p>
                            <div class="newsletter-form">
                                <a href="sign_up/sign_up.php">
                                    <p><input type="submit" class="btn btn-primary" value="Đăng Kí"></p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- End footer top area -->
        <div class="footer-bottom-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="copyright">
                            <p>&copy; 2019 Lập Trình Web. All Rights Reserved. <a href="#"
                                    target="_blank">Nhóm 5</a></p>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="footer-card-icon">
                            <i class="fa fa-cc-discover"></i>
                            <i class="fa fa-cc-mastercard"></i>
                            <i class="fa fa-cc-paypal"></i>
                            <i class="fa fa-cc-visa"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- End footer bottom area -->
