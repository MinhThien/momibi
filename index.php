<?php
include('connect.php');
mysqli_set_charset($conn,'utf8');

session_start(); 

if (isset($_GET['logout'])) {
session_destroy();
unset($_SESSION['email']);
header("location: index.php");
}

if (isset($_SESSION['email'])) {
    $_email = $_SESSION['email'];
    $user = $conn->query("SELECT * FROM customers WHERE email='$_email'");
    $row_user = mysqli_fetch_assoc($user);
}
?>
<!DOCTYPE php>
<php lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Trang chủ</title>
    <link rel="stylesheet" type="text/css" href="./css/ass1.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Bootstrap -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
    <script src="http://cdn.jsdelivr.net/jquery.slick/1.3.15/slick.min.js"></script>

    <script>
        
        $(document).ready(function () {
            $('.slider').bxSlider();
        });

        $(document).ready(function () {
            $("a[href*='#']:not([href='#])").click(function () {
                let target = $(this).attr("href");
                $('html,body').stop().animate({
                    scrollTop: $(target).offset().top
                }, 1000);
                event.preventDefault();
            });
        });
    </script>
    <script>
        $(document).ready(function(){
            $('.customer-logos').slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 500,
                arrows: false,
                dots: false,
                    pauseOnHover: true,
                    responsive: [{
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 3
                    }
                }, {
                    breakpoint: 520,
                    settings: {
                        slidesToShow: 2
                    }
                }]
            });
        });
    </script>

</head>

<body>

    <div class="containers">

        <div id="header">
            <div class="inner-header">
                <div class="slider-area d-none d-lg-block">
                    <div class="block-slider block-slider4">
                        <ul class="slider" id="bxslider-home1">
                        <li>
                                <img src="img/h4-slide.png" alt="Slide">
                            </li>
                            <li><img src="img/h4-slide3.png" alt="Slide">
                            </li>
                            <li><img src="img/h4-slide7.png" alt="Slide">

                            </li>
                            <li><img src="img/h4-slide4_1.png" alt="Slide">

                            </li>
                            
                        </ul>
                    </div>
                </div> <!-- End slider area -->
                <!-- <div class="d-block d-lg-none temp-header"></div>
                <div class="test-header ">
                            <div class="logo-container ">
                                    <a href="index.php"><img src="img/mimobi.png" alt="" height="87"></a>
                                </div>
                                <div class="header-button d-none d-sm-block">
                                    <a href="search.php"><i class="fa fa-search searching" style="font-size: 25px; padding-right: 20px;"></i></a>
                                    <a href="sign_up/sign_in.php" class="btn btn-white btn-animation-1 login-button">Đăng Nhập</a>
                                    <a href="sign_up/sign_up.php" class="btn btn-white btn-animation-1 signup-button">Đăng Kí</a>
                                </div>
                    
                </div> -->
                <div class="d-block d-lg-none temp-header"></div>
                <div class="test-header">
                    <div class="logo-container d-none d-sm-block">
                        <a href="index.php"><img src="img/mimobi.png" alt="" height="87"></a>
                    </div>
                    <div
                     class="logo-container d-sm-none">
                        <a href="index.php"><img src="img/mimobismall.png" alt="" height="87"></a>
                    </div>
                    <div class="header-button">
                        <a href="search.php"><i class="fa fa-search searching" style="font-size: 25px; padding-right: 20px;"></i></a>
                        <?php  if (isset($_SESSION['email'])) : ?>
                            <a href="user/user_page.php"><?php echo $row_user['last_name']; ?></a>
                            <a href="index.php?logout='1'" class="btn btn-white btn-animation-1 login-button">Đăng Xuất </a>
                        <?php else : ?>
                            <a href="sign_up/sign_in.php" class="btn btn-white btn-animation-1 login-button" id="signin">Đăng Nhập</a>
                            <a href="sign_up/sign_up.php" class="btn btn-white btn-animation-1 signup-button" id="signup">Đăng Ký</a>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="body">
            <div class="brands-area">
                <!-- <div class="row fix"> -->
                    <div class="customer-logos">
                        <div class="slide"><a href="#Nokia"><img src="img/brand1.png" alt=""></a></div>
                        <div class="slide"><a href="allproductspage.php#all-xiaomi"><img src="img/brand2.png" alt=""></a></div>
                        <div class="slide"><a href="#SamSung"><img src="img/brand3.png" alt=""></a></div>
                        <div class="slide"><a href="#Apple"><img src="img/brand4.png" alt=""></a></div>
                        <div class="slide"><a href="allproductspage.php#all-oppo"><img src="img/brand5.png" alt=""></a></div>
                    </div>
                <!-- </div> -->
            </div>
            <!-- <div class="header-button d-block d-sm-none fix-mg">
                    <a href="search.php"><i class="fa fa-search searching" style="font-size: 25px; padding-right: 20px;"></i></a>
                    <a href="sign_up/sign_in.php" class="btn btn-white btn-animation-1 login-button">Đăng Nhập</a>
                    <a href="sign_up/sign_up.php" class="btn btn-white btn-animation-1 signup-button test-signup">Đăng Kí</a>
                </div> -->
            <div class="brands-area-1" style="padding-bottom:0px">
                <h2 class="caption title fix-color" style="font-family: Arial, Helvetica, sans-serif; font-weight: bold;">
                    SẢN PHẨM <span class="primary">NỔI BẬT</span>
                </h2>
                <div class="row fix">
                <?php
                
                        //get rows query
                        $query = $conn->query("SELECT * FROM products WHERE status=1 ORDER BY id DESC LIMIT 3");
                        if($query->num_rows > 0){ 
                            while($row = $query->fetch_assoc()){
                                $myId = $row['id'];
                                
                        ?>
                    <div class="col-4">
                        <div class="item">
                            <div class="item-container-image">
                            <img src="products/<?php echo $row["detail_img1"]; ?>">
                            </div>
                            <div class="item-content">
                                <div class="item-name">
                                    <span class="item-name-child"><?php echo $row["name"]; ?></span>
                                </div>

                                <div class="item-price">
                                    <p class="item-price-child"><?php echo number_format($row["price"]).' VND'; ?></p>
                                </div>
                                <div class="item-detail this-button">
                                    <span class="item-detail-child">trả góp 0% khuyến mãi tặng sạc dự phòng</span>
                                </div>
                                <div class="item-status">
                                    <!-- <a href="cartAction.php?id="
                                        class="btn btn-white btn-animation-1 login-button fix-pos this-button">Thêm vào
                                        giỏ hàng</a> -->
                                    <?php echo '<a href="cartAction.php?id=',$myId,'" class="btn btn-white btn-animation-1 login-button fix-pos this-button">Thêm vào giỏ hàng</a>'; ?>
                                    <?php echo '<a href="detail.php?id=',$myId,'" class="btn btn-white btn-animation-1 signup-button fix-pos">Chi tiết</a>'; ?>
                                    
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } }else{ ?>
                <?php } ?>
            </div>

            <div class="product-1 rm-padding " id="Apple">
                <div class="box-middle">
                    <div class="box-clear">
                        <div class="box-caption">
                            <div class="form-box">
                                <img src="img/brand4.png" alt="">
                                <hr class="divider-box restyle-divider-box">
                                <p class="sub-text black-color">Iphone</p>
                                <p class="sub-text black-color">Hãng điện thoại thông minh số một thế giới</p>
                            </div>
                            <div class="form-group">
                                <a href="allproductspage.php#all-apple" class="btn btn-white btn-animation-1 button-product fix-button">Tất cả sản phẩm</a>
                            </div>
                        </div>
                        <div class="box-img d-lg-block d-none">
                            <div class="slider-area">
                                <div class="block-slider block-slider4 control-slider">
                                    <ul class="slider" id="bxslider-home2">
                                    <?php
                                    //get rows query
                                    $query = $conn->query("SELECT * FROM products WHERE status=1 AND brand = 'Apple'");
                                    if($query->num_rows > 0){ 
                                        while($row = $query->fetch_assoc()){
                                            $Id = $row["id"];
                                    ?>
                                        <li>
                                            <img src="products/<?php echo $row["avatar"]; ?>">
                                            <div class="caption-group">
                                                <h2 class="caption title fix-color">
                                                <span class="primary"><?php echo $row["name"]; ?></span>
                                                </h2>
                                                <p class="item-price-child"><?php echo number_format($row["price"]).' VND'; ?></p>
                                                <?php echo '<a href="cartAction.php?id=',$Id,'" class="btn btn-white btn-animation-1 login-button fix-pos this-button">Thêm vào giỏ hàng</a>'; ?>
                                            </div>

                                        </li>
                                        <?php } }else{ ?>
                                    <p>Product(s) not found.....</p>
                                    <?php } ?>
                                        
                                    </ul>
                                </div>
                            </div> <!-- End slider area -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="brands-area-1 ">
                <h2 class="caption title fix-color" style="font-family: Arial, Helvetica, sans-serif; font-weight: bold;">
                    SẢN PHẨM <span class="primary">MỚI</span>
                </h2>
                <div class="row fix">
                <?php
                        //get rows query
                        $query = $conn->query("SELECT * FROM products WHERE status=1 ORDER BY id < 9 DESC LIMIT 3");
                        if($query->num_rows > 0){ 
                            while($row = $query->fetch_assoc()){
                                $Id = $row["id"];
                        ?>
                    <div class="col-4">
                        <div class="item">
                            <div class="item-container-image">
                            <img src="products/<?php echo $row["detail_img1"]; ?>">
                            </div>
                            <div class="item-content">
                                <div class="item-name">
                                    <span class="item-name-child"><?php echo $row["name"]; ?></span>
                                </div>

                                <div class="item-price">
                                    <p class="item-price-child"><?php echo number_format($row["price"]).' VND'; ?></p>
                                </div>
                                <div class="item-detail this-button">
                                    <span class="item-detail-child">trả góp 0% khuyến mãi tặng sạc dự phòng</span>
                                </div>
                                <div class="item-status">
                                <?php echo '<a href="cartAction.php?id=',$Id,'" class="btn btn-white btn-animation-1 login-button fix-pos this-button">Thêm vào giỏ hàng</a>'; ?>
                                    <?php echo '<a href="detail.php?id=',$Id,'" class="btn btn-white btn-animation-1 signup-button fix-pos">Chi tiết</a>'; ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } }else{ ?>
                <?php } ?>
            </div>


            <div class="product-1 rm-padding" id="SamSung">
                <div class="box-middle">
                    <div class="box-clear">
                        <div class="box-caption">
                            <div class="form-box">
                                <img src="img/brand3.png" alt="">
                                <hr class="divider-box restyle-divider-box">
                                <p class="sub-text black-color">SamSung</p>
                                <p class="sub-text black-color">Hãng điện thoại thông minh số hai thế giới</p>
                            </div>
                            <div class="form-group">
                                <a href="allproductspage.php#all-samsung" class="btn btn-white btn-animation-1 button-product">Tất cả sản phẩm</a>
                            </div>
                        </div>
                        <div class="box-img d-lg-block d-none">
                            <div class="slider-area">
                                <div class="block-slider block-slider4 control-slider">
                                <ul class="slider" id="bxslider-home2">
                                    <?php
                                    //get rows query
                                    $query = $conn->query("SELECT * FROM products WHERE status=1 AND brand = 'Samsung'");
                                    if($query->num_rows > 0){ 
                                        while($row = $query->fetch_assoc()){
                                            $Id = $row["id"];
                                    ?>
                                        <li>
                                        <img src="products/<?php echo $row["avatar"]; ?>">
                                            <div class="caption-group">
                                                <h2 class="caption title fix-color">
                                                <span class="primary"><?php echo $row["name"]; ?></span>
                                                </h2>
                                                <p class="item-price-child"><?php echo number_format($row["price"]).' VND'; ?></p>
                                                <?php echo '<a href="cartAction.php?id=',$Id,'" class="btn btn-white btn-animation-1 login-button fix-pos this-button">Thêm vào giỏ hàng</a>'; ?>
                                            </div>

                                        </li>
                                        <?php } }else{ ?>
                                    <p>Product(s) not found.....</p>
                                    <?php } ?>
                                        
                                    </ul>
                                </div>
                            </div> <!-- End slider area -->
                        </div>
                    </div>
                </div>
            </div>

            <div class="promo-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 col-sm-6">
                            <div class="single-promo promo1">
                                <i class="fa fa-reply pdri"></i>
                                <p>Hoàn Trả Trong 30 Ngày</p>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="single-promo promo2">
                                <i class="fa fa-truck"></i>
                                <p>Giao Hàng Hoàn Toàn Miễn Phí</p>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="single-promo promo3">
                                <i class="fa fa-lock"></i>
                                <p>Thanh Toán Tiện Dụng</p>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="single-promo promo4">
                                <i class="fa fa-gift"></i>
                                <p>Nhiều Quà Tặng Khuyến Mãi</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="product-1 rm-padding " id="Nokia">
                <div class="box-middle">
                    <div class="box-clear">
                        <div class="box-caption">
                            <div class="form-box">
                                <img src="img/brand1.png" alt="">
                                <hr class="divider-box restyle-divider-box">
                                <p class="sub-text black-color">Nokia</p>
                                <p class="sub-text black-color">Hãng điện thoại thông minh số ba thế giới</p>
                            </div>
                            <div class="form-group">
                                <a href="allproductspage.php#all-nokia" class="btn btn-white btn-animation-1 button-product">Tất cả sản phẩm</a>
                            </div>
                        </div>
                        <div class="box-img d-lg-block d-none">
                            <div class="slider-area">
                                <div class="block-slider block-slider4 control-slider">
                                <ul class="slider" id="bxslider-home2">
                                    <?php
                                    //get rows query
                                    $query = $conn->query("SELECT * FROM products WHERE status=1 AND brand = 'Nokia'");
                                    if($query->num_rows > 0){ 
                                        while($row = $query->fetch_assoc()){
                                            $Id = $row["id"];
                                    ?>
                                        <li>
                                        <img src="products/<?php echo $row["avatar"]; ?>">
                                            <div class="caption-group">
                                                <h2 class="caption title fix-color">
                                                <span class="primary"><?php echo $row["name"]; ?></span>
                                                </h2>
                                                <p class="item-price-child"><?php echo number_format($row["price"]).' VND'; ?></p>
                                                <?php echo '<a href="cartAction.php?id=',$Id,'" class="btn btn-white btn-animation-1 login-button fix-pos this-button">Thêm vào giỏ hàng</a>'; ?>
                                            </div>

                                        </li>
                                        <?php } }else{ ?>
                                    <p>Product(s) not found.....</p>
                                    <?php } ?>
                                        
                                    </ul>
                                </div>
                            </div> <!-- End slider area -->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
            include('footer.php');
        ?>

    </div>
</body>

</html>