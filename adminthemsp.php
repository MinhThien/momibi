<?php
    include('connect.php');
    //php can load vietnamese form server
    mysqli_set_charset($conn,'utf8');

    session_start();
    //echo ($_SESSION['admin']);
    if (!isset($_SESSION['admin'])) {
        exit;
    }
?>
<!DOCTYPE php>
<php lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Trang Sản Phẩm</title>
    <!-- Bootstrap -->
    <link href="./css/bootstrap.css" rel="stylesheet">
    <script src="./js/jquery-3.3.1.slim.min.js"></script>
    <script src="./js/jquery-3.4.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <!-- Font awesome -->
    <link href="./css/all.css" rel="stylesheet">
    <!-- Moris chart -->
    <link rel="stylesheet" href="./css/moris/morris-0.4.3.min.css">
    <!-- Data Table -->
    <link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css">
    <!-- Style -->
    <link rel="stylesheet" href="./css/adminstyle.css">

</head>
<body>
    <div class="container-fluid">
        <nav class="navbar navbar-expand-md bg-dark navbar-dark">
            <a class="navbar-brand col-md-3 navbarabove" style="margin-right: 0px;" href="admindashboard.php">MIMOBI</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar" style="outline:none">
                <span class="navbar-toggler-icon"></span>
            </button>
        </nav>
        
        <div class="collapse navbar-collapse sidecolor d-md-none" id="collapsibleNavbar">
            <!-- d-md-none is div hide on larger than md -->
            <ul class="nav" id="main-menu">
                <li class="text-center adminavatar">
                    <img src="./img/admin.png" alt="admin" width="70" height="70"/>
                    <p style="margin-top: 10px; margin-bottom:0px; font-size: 20px">Xin chào, Admin</p>
                </li>
                <li><a href="admin.php" class="btn btn-danger square-btn-adjust" style="width: 60px; height: 30px; margin: 10px auto; padding:0">Thoát</a> </li>
                <li><a href="admindashboard.php"><i class="fa fa-th fa-2x" style="margin: 5px"></i>Dashboard</a></li>
                <li><a href="adminsanpham.php"><i class="fa fa-mobile-alt fa-2x" style="margin: 5px"></i>Sản phẩm</a></li>
                <li><a href="adminnguoidung.php"><i class="fa fa-users-cog fa-2x" style="margin: 5px"></i>Người dùng</a></li>
                <li><a href="admindonhang.php"><i class="fa fa-box-open fa-2x" style="margin: 5px"></i>Đơn hàng</a></li>	              	
            </ul>
        </div> 

        <div class="row">
            <!-- hide div when screen less than md -->
            <div class="col-md-3 d-none d-md-block sidecolor">
                <!-- <nav class="navbar-default navbar-side" role="navigation"> -->
                    <div class="sidebar-collapse" id="sidebar-collapse">
                        <ul class="nav">
                            <li class="text-center adminavatar">
                                <img src="./img/admin.png" alt="admin" width="70" height="70"/>
                                <p style="margin-top: 10px; margin-bottom:0px; font-size: 20px">Xin chào, Admin</p>
                            </li>
                            <li><a href="admin.php" class="btn btn-danger square-btn-adjust" style="width: 60px; height: 30px; margin: 10px auto; padding:0">Thoát</a> </li>
                            <li><a href="admindashboard.php"><i class="fa fa-th fa-2x" style="margin: 5px"></i>Dashboard</a></li>
                            <li><a href="adminsanpham.php"><i class="fa fa-mobile-alt fa-2x" style="margin: 5px"></i>Sản phẩm</a></li>
                            <li><a href="adminnguoidung.php"><i class="fa fa-users-cog fa-2x" style="margin: 5px"></i>Người dùng</a></li>
                            <li><a href="admindonhang.php"><i class="fa fa-box-open fa-2x" style="margin: 5px"></i>Đơn hàng</a></li>	              	
                        </ul>
                    </div>
                <!-- </nav> -->
            </div>
            <div class="col-md-9" style="padding:0">
                <div id="page-wrapper" >
                    <div id="page-inner">
                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <h2 style="color:#f00">Thêm Sản phẩm</h2>   
                            </div>
                        </div>              

                        <hr />

                        <div class="row" style="color:black">
                            <form style="width: 100%" method="post" action="adminxulythemsp.php" enctype="multipart/form-data">
                                <div class="form-row">
                                    <div class="form-group col-sm-4">
                                        <label style="font-weight: bold">Mã sản phẩm</label>
                                        <input type="text" class="form-control" name="masanpham" placeholder="Mã sản phẩm" disabled>
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label style="font-weight: bold">Tên sản phẩm</label>
                                        <input type="text" class="form-control" name="tensanpham" placeholder="Tên sản phẩm">
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label style="font-weight: bold">Hãng sản xuất</label>
                                        <select name="country" class="form-control">
                                            <option selected="selected">Apple</option>
                                            <option>Samsung</option>
                                            <option>Oppo</option>
                                            <option>Xiaomi</option>
                                            <option>Nokia</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-sm-4">
                                        <label style="font-weight: bold">Đơn giá</label>
                                        <input type="text" class="form-control" name="dongia" placeholder="Đơn giá">
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label style="font-weight: bold">Số lượng tồn</label>
                                        <input type="text" class="form-control" name="soluongton" placeholder="Số lượng tồn">
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label style="font-weight: bold">Trạng thái</label><br>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" checked="checked" name="trangthai" id="hoatdong" value="1">
                                            <label class="form-check-label">Hoạt động</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="trangthai" id="khonghoatdong" value="0">
                                            <label class="form-check-label">Không hoạt động</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-sm-4">
                                        <label style="font-weight: bold">Camera (MP)</label>
                                        <input type="text" class="form-control" name="camera" placeholder="Camera">
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label style="font-weight: bold">Màn hình (inch)</label>
                                        <input type="text" class="form-control" name="screensize" placeholder="Màn hình">
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label style="font-weight: bold">Hệ điều hành</label>
                                        <select name="os" class="form-control">
                                            <option selected="selected">iOS</option>
                                            <option>Android</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-sm-4">
                                        <label style="font-weight: bold">Bộ nhớ trong (GB)</label>
                                        <input type="text" class="form-control" name="memory" placeholder="Bộ nhớ trong">
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label style="font-weight: bold">Bộ nhớ RAM (GB)</label>
                                        <input type="text" class="form-control" name="ram" placeholder="Bộ nhớ RAM">
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label style="font-weight: bold">Pin (mAh)</label>
                                        <input type="text" class="form-control" name="battery" placeholder="Dung lượng pin">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-sm-4">
                                        <label style="font-weight: bold" id="file-upload">Ảnh đại diện</label>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="anhdaidien">
                                            <label class="custom-file-label" id="file-upload-filename">Choose file</label>
                                        </div>
                                        <!-- <input type="file" name="myFile"> -->
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label style="font-weight: bold">Ảnh chi tiết 1</label>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="anhchitiet1">
                                            <label class="custom-file-label">Choose file</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label style="font-weight: bold">Ảnh chi tiết 2</label>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="anhchitiet2">
                                            <label class="custom-file-label">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label style="font-weight: bold">Mô tả sản phẩm</label>
                                    <textarea class="form-control rounded-0" name="about" rows="10"></textarea>
                                </div>
                                <input type="submit" name="insert" class="btn btn-danger" value="Thêm">
                                <a href="adminsanpham.php" class="btn btn-danger">Hủy</a>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    mysqli_close($conn);
    ?>

    <script>
        // Add the following code if you want the name of the file appear on select
        $(".custom-file-input").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });
    </script>

</body>
</php>