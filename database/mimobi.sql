-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th12 13, 2019 lúc 07:09 AM
-- Phiên bản máy phục vụ: 10.4.6-MariaDB
-- Phiên bản PHP: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `mimobi`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `first_name` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `customers`
--

INSERT INTO `customers` (`id`, `first_name`, `last_name`, `email`, `phone`, `address`, `password`) VALUES
(1, 'Ngô', 'Văn  Chương', 'vanchuong@gmail.com', '0123123456', 'Đà Nẵng', ''),
(2, 'Hà', 'Nam Giang', 'namgiang@gmail.com', '0123123322', 'Đồng Nai', ''),
(3, 'Nguyễn', 'Hòa Bình', 'nguyenhoabinh@gmail.com', '0123123088', 'Cần Thơ', ''),
(4, 'Hoàng', 'Lý Trang', 'lytrang@gmail.com', '0123123123', 'HCM', ''),
(5, 'Nguyễn', 'Bính Hoa', 'hoabinhnguyen@gmail.com', '0123123123', 'HCM', ''),
(6, 'mimobi', 'mimobi', 'mimobi@gmail.com', '0901234589', 'HCM', 'c4ca4238a0b923820dcc509a6f75849b');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `kart_items`
--

CREATE TABLE `kart_items` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `brand` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `detail_img1` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `detail_img2` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `price` float(10,0) NOT NULL,
  `number` int(11) NOT NULL,
  `status` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '1=Active | 0=Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `kart_items`
--

INSERT INTO `kart_items` (`id`, `customer_id`, `name`, `brand`, `avatar`, `detail_img1`, `detail_img2`, `description`, `price`, `number`, `status`) VALUES
(1, 1, 'Iphone 11', 'Apple', '2iphone11.jpg', 'ip11_5.jpg', 'ip11_7.jpg', '1', 21000000, 2, '1'),
(2, 1, 'Samsung Galaxy A9', 'Samsung', 'samsunga9.png', 'ss_a9.jpg', 'ss_a9.jpg', '1', 8200000, 5, '1');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `grand_total` float(10,0) NOT NULL,
  `created` datetime NOT NULL,
  `status` enum('Pending','Completed') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Pending'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `orders`
--

INSERT INTO `orders` (`id`, `customer_id`, `grand_total`, `created`, `status`) VALUES
(1, 1, 50, '2019-11-20 10:39:49', 'Pending'),
(2, 2, 40, '2019-11-20 10:41:13', 'Pending'),
(3, 3, 90, '2019-11-20 10:51:15', 'Pending'),
(4, 4, 40, '2019-11-20 10:57:13', 'Pending'),
(5, 5, 31000000, '2019-11-20 11:41:13', 'Pending');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `order_items`
--

CREATE TABLE `order_items` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `order_items`
--

INSERT INTO `order_items` (`id`, `order_id`, `product_id`, `quantity`) VALUES
(1, 1, 2, 2),
(2, 2, 1, 1),
(3, 2, 2, 1),
(4, 3, 1, 1),
(5, 3, 2, 3),
(6, 4, 2, 1),
(7, 4, 1, 1),
(8, 5, 2, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `brand` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `screensize` float(2,1) NOT NULL,
  `camera` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `ram` int(4) NOT NULL,
  `memory` int(4) NOT NULL,
  `os` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `battery` int(5) NOT NULL,
  `avatar` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `detail_img1` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `detail_img2` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `price` float(10,0) NOT NULL,
  `number` int(11) NOT NULL,
  `status` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '1=Active | 0=Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`id`, `name`, `brand`, `screensize`, `camera`, `ram`, `memory`, `os`, `battery`, `avatar`, `detail_img1`, `detail_img2`, `description`, `price`, `number`, `status`) VALUES
(1, 'Iphone 11', 'Apple', 6.1, 'Trước: 12 MP; Sau: 12MP', 4, 64, 'iOS', 3110, 'iphone.jpg', 'iphone 11.png', 'iphone 11.png', 'iPhone 11 là mẫu iPhone có nhiều màu sắc nhất của Apple được ra mắt trong năm 2019. ', 21000000, 11, '1'),
(2, 'Iphone 11 Pro Max', 'Apple', 6.5, 'Trước: 12 MP; Sau: 12x3MP', 4, 64, 'iOS', 3969, 'iphone.jpg', 'iphone 11 pro max.jpg', 'iphone 11 pro max.jpg', 'iPhone 11 Pro Max là mẫu smartphone cao cấp nhất của Apple được ra mắt năm 2019.', 32000000, 9, '1'),
(3, 'Samsung Galaxy A9', 'Samsung', 6.3, 'Trước: 24 MP; Sau: 24MP', 6, 128, 'Android', 3720, 'samsunga9.png', 'ss_a9.jpg', 'ss_a9.jpg', 'Thiết kế đẹp mắt, kiểu dáng hiện đại và chất lượng hoàn thiện cao cấp giúp điện thoại Galaxy A9 2018 không chỉ thời trang =', 8200000, 6, '1'),
(33, 'Nokia 6.1 Plus', 'Nokia', 5.5, 'Trước: 8 MP; Sau: 16 MP', 32, 32, 'Android', 3000, 'nokia.jpg', 'no_61.jpg', 'no_61.jpg', 'Nokia 6.1 Plus sở hữu cấu hình ấn tượng nhất trong bộ ba smartphone Nokia chạy Android được trình làng hồi đầu năm 2017, ', 3000000, 3, '1'),
(34, 'Oppo Plus', 'Oppo', 6.5, 'Trước: 16 MP; Sau: 48MP', 8, 128, 'Android', 5000, 'oppo.jpg', 'op_a9.jpg', 'op_a9.jpg', 'Đây là mẫu sản phẩm được thừa hưởng những công nghệ mới và tốt nhất ở thời điểm hiện tại so với các đối thủ cùng phân khúc.', 6500000, 4, '1'),
(35, 'OPPO Reno2 ', 'Oppo', 6.5, '16.0 MP', 8, 256, 'Android', 4000, 'oppo.jpg', 'Oppo-Reno.png', 'Oppo-Reno.png', 'Hãy để OPPO Reno2 chắp cánh cho những ý tưởng của bạn. Camera trước vây cá mập độc đáo và 4 camera sau cao cấp mang đến khả ', 14990000, 10, '1'),
(36, 'OPPO F11', 'Oppo', 6.5, '16.0 MP', 16, 128, 'Android', 5000, 'oppo.jpg', 'oppo-f11.jpg', 'oppo-f11.jpg', 'Những chiếc điện thoại của OPPO thường được đánh giá cao ở camera selfie và với OPPO F11 Pro 128GB thì ngoài thế mạnh về camera ', 7490000, 10, '1'),
(37, 'OPPO A5s', 'Oppo', 6.2, '	8.0 MP', 4, 32, 'Android', 2000, 'oppo.jpg', 'oppo a5.jpg', 'oppo a5.jpg', 'OPPO A5s chính hãng là một điện thoại thông minh tuyệt vời, được trang bị rất nhiều tính năng mạnh mẽ. Máy có một màn hình lớn, máy ảnh selfie và quay video sắc nét, dung lượng pin cao và giá bán phải chăng.', 3690000, 10, '1'),
(38, 'Xiaomi Redmi Note 8', 'Xiaomi', 6.5, '20.0 MP', 6, 64, 'Android', 5000, 'Xiaomi-Mi-9-Pro-5G-3.jpg', 'xiaomi-redmi-note-8.jpg', 'xiaomi-redmi-note-8.jpg', 'Redmi Note 8 Pro sở hữu những công nghệ hàng đầu hiện nay. 4 camera 64MP, hiệu năng tuyệt đỉnh, thiết kế hấp dẫn và loạt tính năng mới sẽ khiến bạn phải bất ngờ.', 5990000, 10, '1'),
(39, 'Xiaomi Mi 9', 'Xiaomi', 6.4, '20.0 MP', 6, 64, 'Android', 4000, 'Xiaomi-Mi-9-Pro-5G-3.jpg', 'xiaomi 9.jpg', 'xiaomi 9.jpg', 'Điện thoại cao cấp nhất của Xiaomi đã xuất hiện, Xiaomi Mi 9 với thiết kế tuyệt mỹ, bộ vi xử lý Snapdragon 855 siêu mạnh mẽ và bộ 3 camera 48MP sẽ hoàn toàn chinh phục bạn.', 8990000, 10, '1'),
(40, 'Xiaomi Mi A2 Lite', 'Xiaomi', 6.5, '20.0 MP', 8, 64, 'Android', 2000, 'Xiaomi-Mi-9-Pro-5G-3.jpg', 'xiaomi A2.jpg', 'xiaomi A2.jpg', 'zxczxczxc', 5990000, 10, '1'),
(41, 'Nokia 7.2', 'Nokia', 6.5, '16.0 MP', 6, 64, 'Android', 4000, 'nokia.jpg', 'Nokia-7.2.jpg', 'Nokia-7.2.jpg', 'Đỉnh cao của hoàn thiện và sáng tạo, Nokia 7.2 sở hữu bộ ba camera ZEISS hàng đầu, ', 5990000, 10, '1'),
(42, 'Nokia X6', 'Nokia', 6.5, '20.0 MP', 8, 64, 'Android', 4000, 'nokia.jpg', 'nokia x6.jpg', 'nokia x6.jpg', 'Nokia X6 sẽ kể nên những câu chuyện trong cuộc sống của bạn với camera kép AI tuyệt vời, vi xử lý mạnh mẽ và màn hình tràn viền kiểu mới.\r\n\r\n ', 8990000, 10, '1'),
(43, 'Nokia 3.2', 'Nokia', 5.8, '16.0 MP', 16, 256, 'Android', 5000, 'nokia.jpg', 'nokia-32.jpg', 'nokia-32.jpg', 'Với Nokia 3.2, bạn có thể thỏa sức \"phiêu\" cùng các công nghệ hiện đại mà không cần bỏ ra quá nhiều chi phí.', 14990000, 10, '1'),
(44, 'Samsung Galaxy Note 10 ', 'Samsung', 6.5, '20.0 MP', 16, 256, 'Android', 5000, 'samsunga9.png', 'samsung-galaxy-note.jpg', 'samsung-galaxy-note.jpg', 'Quyền năng thế hệ mới đã xuất hiện, Samsung Galaxy Note 10 mang đến trải nghiệm tuyệt đỉnh như máy tính trong một thiết kế gọn', 14990000, 10, '1'),
(45, 'Samsung Galaxy A70', 'Samsung', 6.5, '16.0 MP', 16, 256, 'Android', 5000, 'samsunga9.png', 'samsung-galaxy-a70.jpg', 'samsung-galaxy-a70.jpg', 'Chiếc điện thoại dành cho những điều lớn: màn hình lớn, viên pin dung lượng cao; 3 camera độ phân giải “siêu khủng”, Samsung A70 mở ra kỷ nguyên mới cho riêng bạn', 3690000, 10, '1');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Chỉ mục cho bảng `kart_items`
--
ALTER TABLE `kart_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Chỉ mục cho bảng `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Chỉ mục cho bảng `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Chỉ mục cho bảng `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `kart_items`
--
ALTER TABLE `kart_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `kart_items`
--
ALTER TABLE `kart_items`
  ADD CONSTRAINT `kart_items_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Các ràng buộc cho bảng `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Các ràng buộc cho bảng `order_items`
--
ALTER TABLE `order_items`
  ADD CONSTRAINT `order_items_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `order_items_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
