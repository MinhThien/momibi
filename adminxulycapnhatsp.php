<?php
    include('connect.php');
    //php can load vietnamese form server
    mysqli_set_charset($conn,'utf8');

    session_start();
    //echo ($_SESSION['admin']);
    if (!isset($_SESSION['admin'])) {
        exit;
    }

    //Get the current id
    $id = $_GET['id'];
    //echo $id;
?>
<!DOCTYPE php>
<php lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Trang Sản Phẩm</title>
    <!-- Bootstrap -->
    <link href="./css/bootstrap.css" rel="stylesheet">
    <script src="./js/jquery-3.3.1.slim.min.js"></script>
    <script src="./js/jquery-3.4.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <!-- Font awesome -->
    <link href="./css/all.css" rel="stylesheet">
    <!-- Moris chart -->
    <link rel="stylesheet" href="./css/moris/morris-0.4.3.min.css">
    <!-- Data Table -->
    <link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css">
    <!-- Style -->
    <link rel="stylesheet" href="./css/adminstyle.css">
</head>
<body>
    <div class="container-fluid">
        <nav class="navbar navbar-expand-md bg-dark navbar-dark">
            <a class="navbar-brand col-md-3 navbarabove" style="margin-right: 0px;" href="admindashboard.php">MIMOBI</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar" style="outline:none">
                <span class="navbar-toggler-icon"></span>
            </button>
        </nav>
        
        <div class="collapse navbar-collapse sidecolor d-md-none" id="collapsibleNavbar">
            <!-- d-md-none is div hide on larger than md -->
            <ul class="nav" id="main-menu">
                <li class="text-center adminavatar">
                    <img src="./img/admin.png" alt="admin" width="70" height="70"/>
                    <p style="margin-top: 10px; margin-bottom:0px; font-size: 20px">Xin chào, Admin</p>
                </li>
                <li><a href="admin.php" class="btn btn-danger square-btn-adjust" style="width: 60px; height: 30px; margin: 10px auto; padding:0">Thoát</a> </li>
                <li><a href="admindashboard.php"><i class="fa fa-th fa-2x" style="margin: 5px"></i>Dashboard</a></li>
                <li><a href="adminsanpham.php"><i class="fa fa-mobile-alt fa-2x" style="margin: 5px"></i>Sản phẩm</a></li>
                <li><a href="adminnguoidung.php"><i class="fa fa-users-cog fa-2x" style="margin: 5px"></i>Người dùng</a></li>
                <li><a href="admindonhang.php"><i class="fa fa-box-open fa-2x" style="margin: 5px"></i>Đơn hàng</a></li>	              	
            </ul>
        </div> 

        <div class="row">
            <!-- hide div when screen less than md -->
            <div class="col-md-3 d-none d-md-block sidecolor">
                <!-- <nav class="navbar-default navbar-side" role="navigation"> -->
                    <div class="sidebar-collapse" id="sidebar-collapse">
                        <ul class="nav">
                            <li class="text-center adminavatar">
                                <img src="./img/admin.png" alt="admin" width="70" height="70"/>
                                <p style="margin-top: 10px; margin-bottom:0px; font-size: 20px">Xin chào, Admin</p>
                            </li>
                            <li><a href="admin.php" class="btn btn-danger square-btn-adjust" style="width: 60px; height: 30px; margin: 10px auto; padding:0">Thoát</a> </li>
                            <li><a href="admindashboard.php"><i class="fa fa-th fa-2x" style="margin: 5px"></i>Dashboard</a></li>
                            <li><a href="adminsanpham.php"><i class="fa fa-mobile-alt fa-2x" style="margin: 5px"></i>Sản phẩm</a></li>
                            <li><a href="adminnguoidung.php"><i class="fa fa-users-cog fa-2x" style="margin: 5px"></i>Người dùng</a></li>
                            <li><a href="admindonhang.php"><i class="fa fa-box-open fa-2x" style="margin: 5px"></i>Đơn hàng</a></li>	              	
                        </ul>
                    </div>
                <!-- </nav> -->
            </div>
            <div class="col-md-9" style="padding:0">
                <div id="page-wrapper" >
                    <div id="page-inner">
                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <h2 style="color:#f00">Cập nhật Sản phẩm</h2>   
                            </div>
                        </div>              

                        <hr />

                        <?php
                            if (isset($_POST['edit'])){
                                $error = array();
                                $product_name               = isset($_POST['tensanpham']) ? $_POST['tensanpham'] : '';
                                $brand                      = isset($_POST['country']) ? $_POST['country'] : '';

                                $screensize                 = isset($_POST['screensize']) ? $_POST['screensize'] : '';
                                $camera                     = isset($_POST['camera']) ? $_POST['camera'] : '';
                                $os                         = isset($_POST['os']) ? $_POST['os'] : '';
                                $memory                     = isset($_POST['memory']) ? $_POST['memory'] : '';
                                $ram                        = isset($_POST['ram']) ? $_POST['ram'] : '';
                                $battery                    = isset($_POST['battery']) ? $_POST['battery'] : '';

                                $price                      = isset($_POST['dongia']) ? $_POST['dongia'] : '';
                                $quantity                   = isset($_POST['soluongton']) ? $_POST['soluongton'] : '';
                                $status                     = isset($_POST['trangthai']) ? $_POST['trangthai'] : '';
                                $about                      = isset($_POST['about']) ? $_POST['about'] : '';



                                $sql = "SELECT * FROM products WHERE id = '$id' ";
                                $result = mysqli_query($conn, $sql);
                                $row = mysqli_fetch_assoc($result);
                            
                                $avatar = $row['avatar'];
                                $detail1 = $row['detail_img1'];
                                $detail2 = $row['detail_img2'];

                                //Kiểm tra name
                                if (empty($product_name)){
                                    $error['product_name'] = 'Name must not be blank <br>';
                                }
                                else if (is_numeric($product_name)){
                                    $error['product_name'] = 'Name must be string <br>';
                                }
                                else if (strlen($product_name) < 5 || strlen($product_name) > 40){
                                    $error['product_name'] = 'Name length must be between 5 and 40 <br>';
                                }

                                //Kiểm tra camera
                                if (empty($camera)){
                                    $error['camera'] = 'Camera must not be blank <br>';
                                }
                                else if (is_numeric($camera)){
                                    $error['camera'] = 'Camera must be string <br>';
                                }
                                else if (strlen($camera) < 5){
                                    $error['camera'] = 'Camera length must be longer than 5 <br>';
                                }

                                //Kiểm tra kích thước màn hình
                                if (empty($screensize)){
                                    $error['sreensize'] = 'Screen size must not be blank <br>';
                                }
                                else if (!is_numeric($screensize) || $screensize<=0){
                                    $error['sreensize'] = 'Screen size must be an integer more than 0 <br>';
                                }

                                //Kiểm tra bộ nhớ
                                if (empty($memory)){
                                    $error['memory'] = 'Memory must not be blank <br>';
                                }
                                else if (!is_numeric($memory) || $memory<=0){
                                    $error['memory'] = 'Memory must be an integer more than 0 <br>';
                                }

                                //Kiểm tra RAM
                                if (empty($ram)){
                                    $error['ram'] = 'RAM must not be blank <br>';
                                }
                                else if (!is_numeric($ram) || $ram<=0){
                                    $error['ram'] = 'RAM must be an integer more than 0 <br>';
                                }

                                //Kiểm tra pin
                                if (empty($battery)){
                                    $error['battery'] = 'Battery must not be blank <br>';
                                }
                                else if (!is_numeric($battery) || $battery<=0){
                                    $error['battery'] = 'Battry must be an integer more than 0 <br>';
                                }

                                //Kiểm tra price
                                if (empty($price)){
                                    $error['price'] = 'Price must not be blank <br>';
                                }
                                else if (!is_numeric($price) || $price<0){
                                    $error['price'] = 'Price must be an integer more than 0 <br>';
                                }

                                //Kiểm tra quantity
                                if (empty($quantity)){
                                    $error['quantity'] = 'Quantity must not be blank <br>';
                                }
                                else if (!is_numeric($quantity) || $quantity<0){
                                    $error['quantity'] = 'Quantity must be an integer more than 0 <br>';
                                }

                                // //Kiểm tra ảnh
                                $expensions= array("jpeg","jpg","png");

                                $file_name = $_FILES['anhdaidien']['name'];
                                $file_tmp = $_FILES['anhdaidien']['tmp_name'];
                                // $file_ext=strtolower(end(explode('.',$file_name))); 
                                $file_ext = pathinfo($file_name,PATHINFO_EXTENSION);

                                if (empty($file_name)){
                                    // $error['avatar'] = 'Avatar must not be blank <br>';
                                }
                                else {
                                    if(in_array($file_ext,$expensions)=== false){
                                        $errors['avatar']="Chỉ hỗ trợ upload file JPEG hoặc PNG.";
                                    }
                                    else {
                                        $path ="products/".$file_name;
                                        $avatar = $file_name;
                                        move_uploaded_file($file_tmp,$path);   
                                    }
                                }

                                
                                $file_name1 = $_FILES['anhchitiet1']['name'];
                                $file_tmp1 = $_FILES['anhchitiet1']['tmp_name'];
                                // $file_ext=strtolower(end(explode('.',$file_name))); 
                                $file_ext1 = pathinfo($file_name1,PATHINFO_EXTENSION);

                                if (empty($file_name1)){
                                    // $error['detail1'] = 'Detail1 must not be blank <br>';
                                }
                                else {
                                    if(in_array($file_ext1,$expensions)=== false){
                                        $errors['detail1']="Chỉ hỗ trợ upload file JPEG hoặc PNG.";
                                    }
                                    else {
                                        $path1 ="products/".$file_name1;
                                        $detail1 = $file_name1;
                                        move_uploaded_file($file_tmp1,$path1);   
                                    }
                                }

                                $file_name2 = $_FILES['anhchitiet2']['name'];
                                $file_tmp2 = $_FILES['anhchitiet2']['tmp_name'];
                                // $file_ext=strtolower(end(explode('.',$file_name))); 
                                $file_ext2 = pathinfo($file_name2,PATHINFO_EXTENSION);


                                if (empty($file_name2)){
                                    // $error['detail2'] = 'Detail2 must not be blank <br>';
                                }
                                else {
                                    if(in_array($file_ext2,$expensions)=== false){
                                        $errors['detail2']="Chỉ hỗ trợ upload file JPEG hoặc PNG.";
                                    }
                                    else {
                                        $path2 ="products/".$file_name2;
                                        $detail2 = $file_name2;
                                        move_uploaded_file($file_tmp2,$path2);   
                                    }
                                }


                                //Kiểm tra about
                                if (is_numeric($about)){
                                    $error['about'] = 'About must be string <br>';
                                }
                                else if (strlen($about) < 5){
                                    $error['about'] = 'About length must be more than 5 <br>';
                                }

                                if ($error){
                                    echo "<div>Có lỗi xảy ra trong quá trình nhập dữ liệu</div>";
                                    // foreach ($error as $key => $value) {
                                    //     echo $key.$value; 
                                    //     echo "<br>";
                                    // } 
                                    echo "<div>Cập nhật sản phẩm không thành công</div><br>";
                                    echo "<a href=\"admincapnhatsp.php?id=".$id."\" class=\"btn btn-danger\">Quay Về</a>";
                                    // echo mysqli_error($conn);
                                }
                                else {
                                    //Nếu đúng insert dữ liệu
                                    $sql = "UPDATE products
                                    SET name ='$product_name', brand ='$brand', ram = '$ram', memory = '$memory', screensize = '$screensize', camera = '$camera', battery = '$battery', avatar = '$avatar', detail_img1 = '$detail1', detail_img2 = '$detail2' , description = '$about', price = '$price', number = '$quantity',status = '$status'
                                    WHERE id = $id";
                                    
                                    // Thực thi câu truy vấn
                                    $result = mysqli_query($conn, $sql);

                                    if ($result){
                                        echo "<div>Cập nhật sản phẩm thành công</div><br>";
                                        echo "<a href=\"adminsanpham.php\" class=\"btn btn-danger\">Quay Về</a>";
                                    }
                                    else {
                                        echo "<div>Có lỗi xảy ra trong quá trình truy vấn dữ liệu</div>";
                                        echo "<div>Cập nhật sản phẩm không thành công</div><br>";
                                        echo "<a href=\"admincapnhatsp.php?id=".$id."\" class=\"btn btn-danger\">Quay Về</a>";
                                        //echo mysqli_error($conn);
                                    }
                                }
                            }
                        mysqli_close($conn);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</php>