<?php
// Kiểm tra định dạng email
function is_email($str) {
    return (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str)) ? FALSE : TRUE;
}
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
    <title>Trang xác nhận đăng nhập</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- Main Style Css -->
    <link rel="stylesheet" href="./style/style_dangnhap.css"/>
</head>

<body class="container" style="background-color: #F1F1F1">
    <div class="col-12 p-3">
        <h1 style="color: #f48225">ĐĂNG NHẬP ADMIN</h1>
        <br><br>
        <?php
        // Vì tên button submit là do-register nên ta sẽ kiểm tra nếu
        // tồn tại key này trong biến toàn cục $_POST thì nghĩa là người 
        // dùng đã click register(submit)
        if (isset($_POST['dangnhap']))
        {
            // Code PHP xử lý validate
            $error = array();
            $data = array();

            $data['emaildn']        = isset($_POST['emaildn']) ? $_POST['emaildn'] : '';
            $data['matkhaudn']      = isset($_POST['matkhaudn']) ? md5($_POST['matkhaudn']) : '';

            //Validate các trường bị blank hoặc không đúng định dạng hoặc mật khẩu không trùng khớp
            // Kiểm tra định dạng dữ liệu
            //chưa nhập email
            if (empty($data['emaildn'])){
                $error['emaildn'] = 'Chưa nhập email <br>';
            }
            //hoặc email không đúng định dạng
            else if (!is_email($data['emaildn'])){
                $error['emaildn'] = 'Email không đúng định dạng <br>';
            }

            //chưa nhập mật khẩu
            if (empty($data['matkhaudn'])){
                $error['matkhaudn'] = 'Chưa nhập mật khẩu đăng ký <br>';
            }

            if($error){
                echo "<h5>Có lỗi trong quá trình nhập dữ liệu</h5>";
            }
            //Không có lỗi trong quá trình nhập
            else if (!$error){
                    if($data['emaildn'] == 'admin@gmail.com' && $data['matkhaudn'] == md5('admin')){
                        session_start(); 
                        $_SESSION['admin'] = 'admin@gmail.com';
                        echo "<h5>Quá trình đăng nhập thành công</h5>";
                        echo '<a class="btn btn-warning" href="admindashboard.php" ><i class="fa fa-user"></i><b>Về Trang Quản Trị Viên</b></a>';
                    }
                    else {
                        echo "<h5>Có lỗi trong quá trình đăng nhập</h5>";
                        echo '<a class="btn btn-warning" href="admin.php"><i class="fa fa-user"></i><b>Trở lại</b></a>';
                    }
                }
            }
        ?>
    </div>
</body>

</html>