<?php
include('connect.php');
mysqli_set_charset($conn,'utf8');

session_start(); 

if (isset($_GET['logout'])) {
session_destroy();
unset($_SESSION['email']);
header("location: index.php");
}

if (isset($_SESSION['email'])) {
    $_email = $_SESSION['email'];
    $user = $conn->query("SELECT * FROM customers WHERE email='$_email'");
    $row_user = mysqli_fetch_assoc($user);
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Trang Sản Phẩm</title>
    <link rel="stylesheet" type="text/css" href="css/ass1.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Bootstrap -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>


    <script>
        $(document).ready(function () {
            $('.slider').bxSlider();
        });

        $(document).ready(function () {
            $("a[href*='#']:not([href='#])").click(function () {
                let target = $(this).attr("href");
                $('html,body').stop().animate({
                    scrollTop: $(target).offset().top
                }, 1000);
                event.preventDefault();
            });
        });
    </script>
</head>

<body>

    <div class="containers">

        <div id="header">
            <div class="inner-header">
            <div class="slider-area d-none d-lg-block">
                    <div class="block-slider block-slider4">
                        <ul class="slider" id="bxslider-home1">
                        <li>
                                <img src="img/h4-slide.png" alt="Slide">
                            </li>
                            <li><img src="img/h4-slide3.png" alt="Slide">
                            </li>
                            <li><img src="img/h4-slide7.png" alt="Slide">

                            </li>
                            <li><img src="img/h4-slide4_1.png" alt="Slide">

                            </li>
                            
                        </ul>
                    </div>
                </div> <!-- End slider area -->
                <!-- <div class="d-block d-lg-none temp-header"></div>
                <div class="test-header ">
                            <div class="logo-container ">
                                    <a href="index.php"><img src="img/mimobi.png" alt="" height="87"></a>
                                </div>
                                <div class="header-button d-none d-sm-block">
                                    <a href="search.php"><i class="fa fa-search searching" style="font-size: 25px; padding-right: 20px;"></i></a>
                                    <a href="sign_up/sign_in.php" class="btn btn-white btn-animation-1 login-button">Đăng Nhập</a>
                                    <a href="sign_up/sign_up.php" class="btn btn-white btn-animation-1 signup-button">Đăng Kí</a>
                                </div>
                    
                </div> -->
                <div class="d-block d-lg-none temp-header"></div>
                <div class="test-header">
                    <div class="logo-container d-none d-sm-block">
                        <a href="index.php"><img src="img/mimobi.png" alt="" height="87"></a>
                    </div>
                    <div
                     class="logo-container d-sm-none">
                        <a href="index.php"><img src="img/mimobismall.png" alt="" height="87"></a>
                    </div>
                    <div class="header-button">
                        <a href="search.php"><i class="fa fa-search searching" style="font-size: 25px; padding-right: 20px;"></i></a>
                        <?php  if (isset($_SESSION['email'])) : ?>
                            <a href="./user/user_page.php"><?php echo $row_user['last_name']; ?></a>
                            <a href="index.php?logout='1'" class="btn btn-white btn-animation-1 login-button">Đăng Xuất </a>
                        <?php else : ?>
                            <a href="sign_up/sign_in.php" class="btn btn-white btn-animation-1 login-button" id="signin">Đăng Nhập</a>
                            <a href="sign_up/sign_up.php" class="btn btn-white btn-animation-1 signup-button" id="signup">Đăng Kí</a>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="all-product-container">
            <div id="all-apple">
                <div class="row rm-margin jumbotron jumbotron-fluid">
                    <h3 class="display-2" style="font-family: Arial, Helvetica Neue, Helvetica, sans-serif;">Apple</h3>
                </div>
                <div class="row rm-margin row-padding">
                <?php
                
                        //get rows query
                        $query = $conn->query("SELECT * FROM products WHERE brand = 'Apple' AND status=1");
                        if($query->num_rows > 0){ 
                            while($row = $query->fetch_assoc()){
                                $myId = $row['id'];
                                
                        ?>
                        <div class="col-sm-12 col-xs-6 col-md-6 col-lg-3">
                            <div class="card-tnq">
                            <img class="card-img-top" src="products/<?php echo $row['detail_img1']; ?>">
                                <div class="card-block">
                                    <h5 class="card-title"><?php echo $row["name"]; ?></h5>
                                    <p class="card-text"><?php echo number_format($row["price"]).' VND'; ?></p>
                                </div>
                            </div>
                            <div class="card-footer" style="border-bottom: 3px solid #e5e5e5;
                                    border-left: 3px solid #e5e5e5;
                                    border-right: 3px solid #e5e5e5;">
                                <?php echo '<a href="detail.php?id=',$myId,'" class="btn btn-white btn-animation-1 login-button fix-pos this-button" style="margin-left: 60px">Chi tiết</a>'; ?>
                            </div>
                        </div>
                        <?php } }else{ ?>
                <?php } ?>
                </div>
            </div>
            <br>
            <br>

            <div id="all-samsung">
                <div class="row rm-margin">
                    <div class="row rm-margin jumbotron jumbotron-fluid width-full">
                    <h2 class="display-1" style="font-family: Arial, Helvetica Neue, Helvetica, sans-serif;">Samsung</h2>
                </div>
                </div>
                <div class="row rm-margin row-padding">
                <?php
                
                //get rows query
                $query = $conn->query("SELECT * FROM products WHERE brand = 'Samsung' AND status=1");
                if($query->num_rows > 0){ 
                    while($row = $query->fetch_assoc()){
                        $myId = $row['id'];
                        
                ?>
                <div class="col-sm-12 col-xs-6 col-md-6 col-lg-3">
                    <div class="card-tnq">
                    <img class="card-img-top" src="products/<?php echo $row['detail_img1']; ?>">
                        <div class="card-block">
                            <h5 class="card-title"><?php echo $row["name"]; ?></h5>
                            <p class="card-text"><?php echo number_format($row["price"]).' VND'; ?></p>
                        </div>
                    </div>
                    <div class="card-footer" style="border-bottom: 3px solid #e5e5e5;
                            border-left: 3px solid #e5e5e5;
                            border-right: 3px solid #e5e5e5;">
                        <?php echo '<a href="detail.php?id=',$myId,'" class="btn btn-white btn-animation-1 login-button fix-pos this-button" style="margin-left: 60px">Chi tiết</a>'; ?>
                    </div>
                </div>
                <?php } }else{ ?>
        <?php } ?>
                </div>
            </div>
            <br>
            <br>

            <div id="all-nokia">
                <div class="row rm-margin">
                    <div class="row rm-margin jumbotron jumbotron-fluid width-full">
                    <h3 class="display-2" style="font-family: Arial, Helvetica Neue, Helvetica, sans-serif;">Nokia</h3>
                </div>
                </div>
                <div class="row rm-margin row-padding">
                <?php
                
                //get rows query
                $query = $conn->query("SELECT * FROM products WHERE brand = 'Nokia' AND status=1");
                if($query->num_rows > 0){ 
                    while($row = $query->fetch_assoc()){
                        $myId = $row['id'];
                        
                ?>
                <div class="col-sm-12 col-xs-6 col-md-6 col-lg-3">
                    <div class="card-tnq">
                    <img class="card-img-top" src="products/<?php echo $row['detail_img1']; ?>">
                        <div class="card-block">
                            <h5 class="card-title"><?php echo $row["name"]; ?></h5>
                            <p class="card-text"><?php echo number_format($row["price"]).' VND'; ?></p>
                        </div>
                    </div>
                    <div class="card-footer" style="border-bottom: 3px solid #e5e5e5;
                            border-left: 3px solid #e5e5e5;
                            border-right: 3px solid #e5e5e5;">
                        <?php echo '<a href="detail.php?id=',$myId,'" class="btn btn-white btn-animation-1 login-button fix-pos this-button" style="margin-left: 60px">Chi tiết</a>'; ?>
                    </div>
                </div>
                <?php } }else{ ?>
        <?php } ?>
                </div>
            </div>
            <br>
            <br>

            <div id="all-xiaomi">
                <div class="row rm-margin">
                    <div class="row rm-margin jumbotron jumbotron-fluid width-full">
                    <h3 class="display-2" style="font-family: Arial, Helvetica Neue, Helvetica, sans-serif;">Xiaomi</h3>
                </div>
                </div>
                <div class="row rm-margin row-padding">
                <?php
                
                //get rows query
                $query = $conn->query("SELECT * FROM products WHERE brand = 'Xiaomi' AND status=1");
                if($query->num_rows > 0){ 
                    while($row = $query->fetch_assoc()){
                        $myId = $row['id'];
                        
                ?>
                <div class="col-sm-12 col-xs-6 col-md-6 col-lg-3">
                    <div class="card-tnq">
                    <img class="card-img-top" src="products/<?php echo $row['detail_img1']; ?>">
                        <div class="card-block">
                            <h5 class="card-title"><?php echo $row["name"]; ?></h5>
                            <p class="card-text"><?php echo number_format($row["price"]).' VND'; ?></p>
                        </div>
                    </div>
                    <div class="card-footer" style="border-bottom: 3px solid #e5e5e5;
                            border-left: 3px solid #e5e5e5;
                            border-right: 3px solid #e5e5e5;">
                        <?php echo '<a href="detail.php?id=',$myId,'" class="btn btn-white btn-animation-1 login-button fix-pos this-button" style="margin-left: 60px">Chi tiết</a>'; ?>
                    </div>
                </div>
                <?php } }else{ ?>
        <?php } ?>
                </div>
            </div>
            <br>
            <br>

            <div id="all-oppo">
                <div class="row rm-margin">
                    <div class="row rm-margin jumbotron jumbotron-fluid width-full">
                    <h3 class="display-2" style="font-family: Arial, Helvetica Neue, Helvetica, sans-serif;">Oppo</h3>
                </div>
                </div>
                <div class="row rm-margin row-padding">
                <?php
                
                //get rows query
                $query = $conn->query("SELECT * FROM products WHERE brand = 'Oppo' AND status=1");
                if($query->num_rows > 0){ 
                    while($row = $query->fetch_assoc()){
                        $myId = $row['id'];
                        
                ?>
                <div class="col-sm-12 col-xs-6 col-md-6 col-lg-3">
                    <div class="card-tnq">
                    <img class="card-img-top" src="products/<?php echo $row['detail_img1']; ?>">
                        <div class="card-block">
                            <h5 class="card-title"><?php echo $row["name"]; ?></h5>
                            <p class="card-text"><?php echo number_format($row["price"]).' VND'; ?></p>
                        </div>
                    </div>
                    <div class="card-footer" style="border-bottom: 3px solid #e5e5e5;
                            border-left: 3px solid #e5e5e5;
                            border-right: 3px solid #e5e5e5;">
                        <?php echo '<a href="detail.php?id=',$myId,'" class="btn btn-white btn-animation-1 login-button fix-pos this-button" style="margin-left: 60px">Chi tiết</a>'; ?>
                    </div>
                </div>
                <?php } }else{ ?>
        <?php } ?>
                </div>
            <div>

        </div>
        <br>
        <br>

        <?php
            include('footer.php');
        ?>

    </div>
</div>
<script>
    $(document).ready(function() {
        $('html, body').hide();

        if (window.location.hash) {
            setTimeout(function() {
                $('html, body').scrollTop(0).show();
                $('html, body').animate({
                    scrollTop: $(window.location.hash).offset().top
                    }, 1000)
            }, 0);
        }
        else {
            $('html, body').show();
        }
    });
</script>
</body>
</html>