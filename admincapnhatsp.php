<?php
    include('connect.php');
    //php can load vietnamese form server
    mysqli_set_charset($conn,'utf8');

    session_start();
    //echo ($_SESSION['admin']);
    if (!isset($_SESSION['admin'])) {
        exit;
    }

    //Get the current id
    $id = $_GET['id'];
    //echo $id;

    //take the current value
    $sql = "SELECT * FROM products WHERE id = '$id' ";
    $result = mysqli_query($conn, $sql);
    $row = mysqli_fetch_assoc($result);

    $product_name = $row['name'];
    $brand = $row['brand'];
    
    $screensize = $row['screensize'];
    $camera = $row['camera'];

    $ram = $row['ram'];
    $memory = $row['memory'];
    $os = $row['os'];
    $battery = $row['battery'];

    $avatar = $row['avatar'];
    $detail1 = $row['detail_img1'];
    $detail2 = $row['detail_img2'];
    $about = $row['description'];

    $price = $row['price'];
    $quantity = $row['number'];
    $status = $row['status'];

    function is_checked($db_value, $html_value){
        if($db_value == $html_value){
            return "checked";
        }
        else{
            return "";
        }
    }

?>

<!DOCTYPE php>
<php lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Trang Sản Phẩm</title>
    <!-- Bootstrap -->
    <link href="./css/bootstrap.css" rel="stylesheet">
    <script src="./js/jquery-3.3.1.slim.min.js"></script>
    <script src="./js/jquery-3.4.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <!-- Font awesome -->
    <link href="./css/all.css" rel="stylesheet">
    <!-- Moris chart -->
    <link rel="stylesheet" href="./css/moris/morris-0.4.3.min.css">
    <!-- Data Table -->
    <link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css">
    <!-- Style -->
    <link rel="stylesheet" href="./css/adminstyle.css">
</head>
<body>
    <div class="container-fluid">
        <nav class="navbar navbar-expand-md bg-dark navbar-dark">
            <a class="navbar-brand col-md-3 navbarabove" style="margin-right: 0px;" href="admindashboard.php">MIMOBI</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar" style="outline:none">
                <span class="navbar-toggler-icon"></span>
            </button>
        </nav>
        
        <div class="collapse navbar-collapse sidecolor d-md-none" id="collapsibleNavbar">
            <!-- d-md-none is div hide on larger than md -->
            <ul class="nav" id="main-menu">
                <li class="text-center adminavatar">
                    <img src="./img/admin.png" alt="admin" width="70" height="70"/>
                    <p style="margin-top: 10px; margin-bottom:0px; font-size: 20px">Xin chào, Admin</p>
                </li>
                <li><a href="admin.php" class="btn btn-danger square-btn-adjust" style="width: 60px; height: 30px; margin: 10px auto; padding:0">Thoát</a> </li>
                <li><a href="admindashboard.php"><i class="fa fa-th fa-2x" style="margin: 5px"></i>Dashboard</a></li>
                <li><a href="adminsanpham.php"><i class="fa fa-mobile-alt fa-2x" style="margin: 5px"></i>Sản phẩm</a></li>
                <li><a href="adminnguoidung.php"><i class="fa fa-users-cog fa-2x" style="margin: 5px"></i>Người dùng</a></li>
                <li><a href="admindonhang.php"><i class="fa fa-box-open fa-2x" style="margin: 5px"></i>Đơn hàng</a></li>	              	
            </ul>
        </div> 

        <div class="row">
            <!-- hide div when screen less than md -->
            <div class="col-md-3 d-none d-md-block sidecolor">
                <!-- <nav class="navbar-default navbar-side" role="navigation"> -->
                    <div class="sidebar-collapse" id="sidebar-collapse">
                        <ul class="nav">
                            <li class="text-center adminavatar">
                                <img src="./img/admin.png" alt="admin" width="70" height="70"/>
                                <p style="margin-top: 10px; margin-bottom:0px; font-size: 20px">Xin chào, Admin</p>
                            </li>
                            <li><a href="admin.php" class="btn btn-danger square-btn-adjust" style="width: 60px; height: 30px; margin: 10px auto; padding:0">Thoát</a> </li>
                            <li><a href="admindashboard.php"><i class="fa fa-th fa-2x" style="margin: 5px"></i>Dashboard</a></li>
                            <li><a href="adminsanpham.php"><i class="fa fa-mobile-alt fa-2x" style="margin: 5px"></i>Sản phẩm</a></li>
                            <li><a href="adminnguoidung.php"><i class="fa fa-users-cog fa-2x" style="margin: 5px"></i>Người dùng</a></li>
                            <li><a href="admindonhang.php"><i class="fa fa-box-open fa-2x" style="margin: 5px"></i>Đơn hàng</a></li>	              	
                        </ul>
                    </div>
                <!-- </nav> -->
            </div>
            <div class="col-md-9" style="padding:0">
                <div id="page-wrapper" >
                    <div id="page-inner">
                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <h2 style="color:#f00">Cập Nhật Sản phẩm</h2>   
                            </div>
                        </div>              

                        <hr />

                        <div class="row" style="color:black">
                            <!-- change action id -->
                            <?php
                            echo "<form style=\"width: 100%\" method=\"post\"  enctype=\"multipart/form-data\" action=\"adminxulycapnhatsp.php?id=$id\">";
                            ?>
                                <div class="form-row">
                                    <div class="form-group col-sm-4">
                                        <label style="font-weight: bold">Mã sản phẩm</label>
                                        <input type="text" class="form-control" name="masanpham" placeholder="Mã sản phẩm" disabled value = "<?php echo $id?>"> 
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label style="font-weight: bold">Tên sản phẩm</label>
                                        <input type="text" class="form-control" name="tensanpham" placeholder="Tên sản phẩm" value = "<?php echo $product_name?>">
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label style="font-weight: bold">Hãng sản xuất</label>
                                        <select name="country" class="form-control">
                                            <option <?php if($brand=="Apple") echo 'selected="selected"'; ?>>Apple</option>
                                            <option <?php if($brand=="Samsung") echo 'selected="selected"'; ?>>Samsung</option>
                                            <option <?php if($brand=="Oppo") echo 'selected="selected"'; ?>>Oppo</option>
                                            <option <?php if($brand=="Xiaomi") echo 'selected="selected"'; ?>>Xiaomi</option>
                                            <option <?php if($brand=="Nokia") echo 'selected="selected"'; ?>>Nokia</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-sm-4">
                                        <label style="font-weight: bold">Đơn giá</label>
                                        <input type="text" class="form-control" name="dongia" value = "<?php echo $price?>">
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label style="font-weight: bold">Số lượng tồn</label>
                                        <input type="text" class="form-control" name="soluongton" value = "<?php echo $quantity?>">
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label style="font-weight: bold">Trạng thái</label><br>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="trangthai" <?php echo is_checked($status, "1"); ?> value="1">
                                            <label class="form-check-label">Hoạt động</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="trangthai" <?php echo is_checked($status, "0"); ?> value="0">
                                            <label class="form-check-label">Không hoạt động</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-sm-4">
                                        <label style="font-weight: bold">Camera (MP)</label>
                                        <input type="text" class="form-control" name="camera" value = "<?php echo $camera?>">
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label style="font-weight: bold">Màn hình (inch)</label>
                                        <input type="text" class="form-control" name="screensize" value = "<?php echo $screensize?>">
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label style="font-weight: bold">Hệ điều hành</label>
                                        <select name="os" class="form-control">
                                            <option <?php if($os=="iOS") echo 'selected="selected"'; ?>>iOS</option>
                                            <option <?php if($os=="Android") echo 'selected="selected"'; ?>>Android</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-sm-4">
                                        <label style="font-weight: bold">Bộ nhớ trong (GB)</label>
                                        <input type="text" class="form-control" name="memory" value = "<?php echo $memory?>">
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label style="font-weight: bold">Bộ nhớ RAM (GB)</label>
                                        <input type="text" class="form-control" name="ram" value = "<?php echo $ram?>">
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label style="font-weight: bold">Pin (mAh)</label>
                                        <input type="text" class="form-control" name="battery" value = "<?php echo $battery?>">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-sm-4">
                                        <label style="font-weight: bold">Ảnh đại diện</label>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="anhdaidien">
                                            <label class="custom-file-label"><?php echo $avatar?></label>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label style="font-weight: bold">Ảnh chi tiết 1</label>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="anhchitiet1">
                                            <label class="custom-file-label"><?php echo $detail1?></label>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label style="font-weight: bold">Ảnh chi tiết 2</label>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="anhchitiet2">
                                            <label class="custom-file-label"><?php echo $detail2?></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label style="font-weight: bold">Mô tả sản phẩm</label>
                                    <textarea class="form-control rounded-0" name="about" rows="10"><?php echo $about?></textarea>
                                </div>
                                <input type="submit" name="edit" class="btn btn-danger" value="Chỉnh sửa">
                                <a href="adminsanpham.php" class="btn btn-danger">Hủy</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    mysqli_close($conn);
    ?>
    <script>
        // Add the following code if you want the name of the file appear on select
        $(".custom-file-input").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });
    </script>
</body>
</php>